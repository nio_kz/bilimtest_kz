<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
$router->get('/links', "IndexController@certificates");
$router->get('/fix-amount', "IndexController@fix_amount");
$router->get('/lesson-plan-fix', "IndexController@lesson_plan_fix");
$router->get('/', "IndexController@index");
$router->get('/qr', 'IndexController@qr');
$router->get('/api/settings', 'IndexController@settings');
$router->post('/login', "IndexController@login");
$router->get('/login_redirect', "IndexController@login_redirect");
$router->get('/logout', "IndexController@logout");
$router->post('/logout', "IndexController@logout");
$router->get('/api-admin/media', "IndexController@media");
$router->get('/api/media', "IndexController@media");

$router->get('/qiwi-terminal', "IndexController@qiwi");
$router->get('/kassa24-terminal', "IndexController@kassa24");
$router->get('/kaspi-terminal', "IndexController@kaspi");

$router->group(['namespace' => 'Guest', 'prefix' => 'api'], function() use ($router)
{
    //PUBIC
    $router->get('/slider/{section}/{is_video}', 'IndexController@slider');
    $router->get('/banner/{section}', 'IndexController@banner');
    $router->get('/konkurs_tab', 'IndexController@konkurs_tab');
    $router->get('/konkurs', 'IndexController@konkurs');
    $router->get('/konkurs/{id}', 'IndexController@konkurs_detail');
    $router->post('/konkurs-like/{id}/{obj}', 'IndexController@konkurs_like');
    $router->get('/konkurs-member', 'IndexController@konkurs_member');
    $router->get('/konkurs-members/{id}', 'IndexController@konkurs_members');
    $router->post('/konkurs-post-member', 'IndexController@post_konkurs_member');
    
    $router->get('/news', 'IndexController@news');
    $router->get('/top-news', 'IndexController@top_news');
    $router->get('/news/{id}', 'IndexController@news_detail');
    $router->get('/home-news', 'IndexController@home_news');
    $router->get('/home-konkurs-member', 'IndexController@konkurs_members_home');
    
    $router->get('/location[/{id}]', 'IndexController@location');
    $router->get('/school/{id}', 'IndexController@school');
    $router->get('/get-city-school', 'IndexController@get_city_school');


    $router->get('/lesson', 'LessonPlanController@lesson');
    $router->get('/classes', 'LessonPlanController@classes');
    $router->get('/plan-type', 'LessonPlanController@plan_type');
    $router->get('/lesson-plan', 'LessonPlanController@lesson_plans_public');
    $router->get('/lesson-plan-redirect/{slug}', 'LessonPlanController@lesson_plan_redirect');
    $router->get('/lesson-plan-detail/{id}', 'LessonPlanController@lesson_plan_detail');

    $router->get('/o_lesson', 'OlimpiadController@o_lesson');
    $router->get('/lesson-olimpiad', 'OlimpiadController@lesson_olimpiad');
    $router->post('/select-olimpiad', 'OlimpiadController@select_olimpiad');
    $router->get('/start', 'OlimpiadController@start');
    $router->get('/question', 'OlimpiadController@question');
    $router->post('/question-update', 'OlimpiadController@question_update');
    $router->post('/olimpiad-finish', 'OlimpiadController@olimpiad_finish');
    


    //AUTH
    $router->get('lesson-plans', 'LessonPlanController@lesson_plans');
    $router->post('lesson-plan-destory', 'LessonPlanController@lesson_plan_destory');
    
    $router->get('lesson-plan-preview', 'LessonPlanController@lesson_plan_preview');
    $router->post('lesson-plan-download', 'LessonPlanController@lesson_plan_download');
    $router->post('single-file', 'LessonPlanController@upload');
    $router->post('remove-file', 'LessonPlanController@upload_remove');
    $router->post('add-lesson-plan', 'LessonPlanController@add_lesson_plan');
    $router->post('lesson-plan-download-certificate/{id}/{type}', 'LessonPlanController@download_certificate');
    $router->get('lesson-plan-download-certificate-html/{id}', 'LessonPlanController@download_certificate_html');

    $router->post('register', 'LoginController@register');
    $router->post('login', 'LoginController@login');

    $router->post('forget-password', 'LoginController@forget_password');
    $router->post('reset-password', 'LoginController@reset_password');
    
    $router->post('me', 'UserController@me');
    $router->post('user', 'UserController@user');
    $router->get('unfinished', 'UserController@unfinished');
    $router->post('profile-post', 'UserController@profile_post');
    $router->get('get-user-school', 'UserController@user_school');
    $router->get('/user/transaction', 'UserController@transaction');
    $router->get('/user/transfer', 'UserController@transfer');
    $router->post('/user/transfer-post', 'UserController@transfer_post');
    $router->get('/user/my-result', 'UserController@my_result_year');
    $router->get('/user/result/{token}', 'UserController@result');

    $router->get('/user/certificates', 'UserController@certificates');
    $router->get('/user/certificate/{type}/{token}', 'UserController@certificate');
    $router->get('/user/certificate-html/{token}/{user_token}', 'UserController@certificate_html');


    $router->get('/user/konkurs-certificate/{type}/{id}/pos/{pos}', 'UserController@konkurs_certificate');
    $router->get('/user/konkurs-certificate-html/{id}/{user_id}', 'UserController@konkurs_certificate_html');

    $router->get('/konkurs-edit-member', 'UserController@get_konkurs_member');
    $router->post('/konkurs-update-member', 'UserController@update_konkurs_member');
    $router->post('/konkurs-remove-member-media', 'UserController@remove_media_konkurs_member');
    $router->post('/konkurs-remove-member', 'UserController@remove_konkurs_member');

    $router->get('/user/invoice', 'UserController@invoice');
    $router->post('/user/invoice-post', 'UserController@invoice_post');
    $router->post('/user/invoice-destory', 'UserController@invoice_destory');
    
    $router->get('/user/konkurs', 'UserController@konkurs');

    $router->post('/user/add-student', 'AddUserController@add_student');
    $router->get('/user/my-student', 'AddUserController@my_student');
    $router->get('/user/get-olimpiad', 'AddUserController@get_olimpiad');
    

    $router->post('paybox', 'UserController@paybox');
    $router->post('pult24', 'UserController@pult24');
    $router->post('callback', 'UserController@callback_pult24');
    $router->post('kaspi-online', 'UserController@kaspi_online');

    $router->get('/user/gramota', 'UserController@gramota_list');
    $router->post('/user/gramota', 'UserController@gramota_store');

    $router->get('/user/thanks', 'UserController@thanks_list');
    $router->post('/user/thanks', 'UserController@thanks_store');

    $router->get('/user/gramota-thanks/{type}/{id}', 'UserController@certificate_gramota_thanks');
    $router->get('/user/certificate-gt-html/{id}/{user_id}', 'UserController@certificate_gt_html');

    $router->post('/upload-summernote', 'UserController@summernote_upload');

    $router->post('change-password', 'UserController@change_password');

});


$router->group(['namespace' => 'Backend', 'prefix' => 'api-admin'], function() use ($router)
{

    $router->post('start-olympiad', "QuizController@check_create");

    $router->get('unfinished-quizes', "QuizController@unfinished");




    $router->get('get_list/{table}', "MasterController@get_list");
    $router->post('add_or_update/{table}', "MasterController@add_or_update");
    $router->post('destory/{table}', "MasterController@destory");
    

    $router->get('random-question/randomise', "RandomQuestionController@randomise");
    $router->get('random-question/old-question', "RandomQuestionController@old_questions");
    $router->post('random-question/publish', "RandomQuestionController@publish");
    
    $router->post('upload', "CertificateController@upload");




    // // quizes
    $router->post('quiz/finish', "QuizController@finish");
    $router->get('quiz/question', "QuizController@quiz_question");
    $router->post('quiz/question-update', "QuizController@quiz_question_update");
    $router->get('quizes/{id}', "QuizController@quizes");
    $router->post('recalc/{id}', "QuizController@recalc");
    $router->get('quiz', "QuizController@quiz");
    // $router->post('quiz-update', "QuizController@update");

    // // users
    $router->get('my-results', "UserController@my_results");
    $router->get('my-result-item', "UserController@my_result_item");
    $router->get('quiz-result', "UserController@quiz_result");
    $router->get('user-account', "UserController@account");
    $router->get('user-transfer', "UserController@transfer");
    $router->post('user-transfer', "UserController@transfer_store");
    $router->post('upload-invoice', "UserController@upload");
    $router->post('invoice-store', "UserController@invoice_store");
    $router->post('invoice-remove', "UserController@invoice_remove");
    $router->get('user-invoice', "UserController@invoice");
    $router->get('user-invoice', "UserController@invoice");
    $router->get('report', "ReportController@report");


    // payment qiwi

    $router->post('qiwi/charge', "QiwiController@charge");


    $router->get('user-students', "AddUserController@list");
    $router->post('user-student-add', "AddUserController@add_new");
    $router->get('user-class-lessons', "AddUserController@get_class_lesson");
    $router->post('user-students-create-quiz', "AddUserController@create_quiz");


    $router->post('random-quesiton', 'ClassLessonQuestionsController@randomQuestion');
    $router->post('old-random-quesiton', 'ClassLessonQuestionsController@oldQuestion');

    $router->post('upload-summernote', "CertificateController@summernote_upload");


});
