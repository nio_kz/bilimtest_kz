<?php
/**
 * Created by PhpStorm.
 * User: khnurtai
 * Date: 8/17/18
 * Time: 16:40
 */

return [
    'defaults' => [
        'guard' => 'api',
        'provider' => 'users'
    ],
    'guards' => [
        'api' => [
            'driver' => 'jwt',
            'provider' => 'users'
        ]
    ],
    'providers'=>[
        'users' => [
            'driver' => 'eloquent',
            'model' => \App\Models\User::class,
        ]
    ],
    'passwords' => [
        //
    ]
];