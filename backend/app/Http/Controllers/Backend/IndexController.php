<?php 

namespace App\Http\Controllers\Backend;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;


class PublicController extends Controller
{
    
    public function logout(Request $request){
        $this->guard()->logout();
        return response()->json([
            	'status'=>"success"
            ]);
    }

    public function me(){

        return response()->json(['user'=>$this->guard()->user()]);
    }

    
    public function guard(){
        return Auth::guard();
    }
}
    ?>