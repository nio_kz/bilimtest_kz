<?php 

namespace App\Http\Controllers\Backend;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;


class QuizController extends Controller
{

    public function quizes($id){
        $items = DB::table("o_user_olimpiad")->where("o_user_olimpiad.user_id", $id)->paginate(20);

        return response()->json($items);
    }

    public function recalc($id){
        $item = DB::table("o_user_olimpiad")->where("id", $id)->first();
        $this->calculate_u_0($item->token);
        return response()->json(['status'=>"success"]);
    }

    private function calculate_u_0($token)
    {
       $u_o = DB::table('o_user_olimpiad')->where('token', $token)->first();
       if($u_o){
           $items = DB::table("o_user_olimpiad_question")->where('olimpiad_id', $u_o->id)->get();
           $max_point = $point = 0;
           foreach($items as $item){
                $_q = DB::table("o_question")->where('id', $item->question_id)->first();
                $qs = DB::table("o_user_olimpiad_q_answer")->where('user_olimpiad_question_id', $item->id)->get();
                if($_q->q_type=="cross"){
                    foreach($qs as $q){
                        if($q->col==1){
                            foreach($qs as $qq){
                                if($qq->col==2){
                                    if($q->correct_answer_id == $qq->answer_id){
                                        if($qq->correct_answer_id == $q->answer_id){
                                            $as = DB::table("o_question_answer")->where('id', $q->answer_id)->first();
                                            $point = $point + $as->point;          
                                        }
                                    }
                                }
                            }
                        }
                    }
                }else{
                    foreach($qs as $q){
                        $as = DB::table("o_question_answer")->where('id', $q->answer_id)->first();
                        $max_point = $max_point + $as->point;
                        if($q->checked==1){
                            $point = $point + $as->point;
                        }
                    }
                }
           }
           DB::table('o_user_olimpiad')->where('token', $token)->update(['max_point'=>$max_point, 'point'=>$point]);
       }

    }

    

}