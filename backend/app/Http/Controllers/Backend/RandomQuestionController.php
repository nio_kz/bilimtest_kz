<?php

namespace App\Http\Controllers\Backend;

use App\QuizCertificate;
use App\Quizes;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

use App\Http\Controllers\Controller;

class RandomQuestionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function old_questions(Request $request){
        $filter = $request->all();
        $data = DB::table("o_random_question");
        $data = $data->where('deleted', 0);
        $data = $data->join("o_question", "o_question.id","=", "o_random_question.question_id");
        if(isset($req['filter'])){
            foreach(json_decode($req['filter']) as $key=>$value){
                $data = $data->where("o_random_question.".$key, $value); 
            }
            
        }
        $data = $data->select("o_random_question.*", "o_question.name")->get();
        return response()->json($data);
        
    }

    public function randomise(Request $request){
        $data=[];
        $param = $request->all();
        $q = DB::table("o_question")
        ->where('lesson_id', $param['lesson_id'])
        ->where('class_id', $param['class_id'])
        ->where('deleted', 0);
        if(isset($param['start_at'])&&isset($param['end_at'])){
            $st = strtotime($param['start_at']);
            $et = strtotime($param['end_at']);
            $q = $q->whereRaw("DATE_FORMAT(created_at, '%Y%m%d') between ".date('Ymd',$st)." and ".date('Ymd',$et));
        }
        $q = $q->inRandomOrder()
        ->limit($param['max'])
        ->select("id", "class_id", "lesson_id", "name")
        ->get();
        return response()->json($q);
        // DB::table("random_question")->where('class_id', $param['cls'])
        // ->where('lesson_id', $param['lesson'])->delete();
        // foreach ($q as $_q) {
        //     DB::table("random_question")->insert([
        //         'class_id'=>$param['cls'],
        //         'lesson_id'=>$param['lesson'],
        //         'question_id'=>$_q->id
        //     ]); 
        //     $data[]=[
        //         'id'=>$_q->id,
        //         'name_kz'=>$_q->name_kz,
        //         'name_ru'=>$_q->name_ru
        //     ];
        // }
        return response()->json($data);
    }

    public function publish(Request $request){
        $data=[];
        $param = $request->all();
        $f = $param['f'];
        $q = $param['q'];
        DB::table("o_random_question")->where('class_id', $f['class_id'])
        ->where('lesson_id', $f['lesson_id'])->delete();
        foreach ($q as $_q) {
            DB::table("o_random_question")->insert([
                'class_id'=>$_q['class_id'],
                'lesson_id'=>$_q['lesson_id'],
                'question_id'=>$_q['id']
            ]); 
        }
        return response()->json(["status"=>"success"]);
    }
}