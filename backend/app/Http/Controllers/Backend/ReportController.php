<?php 
// SELECT SUM(amount) as total, source FROM `transaction`
// where status='income' and DATE_FORMAT(created_at, "%Y%m%d") BETWEEN 20191024 and 20191110
// GROUP by source

namespace App\Http\Controllers\Backend;

use App\QuizCertificate;
use App\Quizes;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ReportController extends Controller
{
    public function report(Request $request){
        $data = $request->all();
        $items = [];
        if(isset($data['start_at'])&&isset($data['end_at'])){
            $items = DB::table("transaction")
            ->where('status', 'income')
            ->whereRaw("DATE_FORMAT(created_at, '%Y%m%d') BETWEEN ".intval(str_replace("-", "", $data['start_at'])) ." and ".intval(str_replace("-", "", $data['end_at'])))
            ->groupBy('source')
            ->select(DB::raw("SUM(amount) as total, source"))->get();
        }else{
            $items = DB::table("transaction")
            ->where('status', 'income')
            // ->whereRaw("DATE_FORMAT(created_at, '%Y%m%d') BETWEEN 20191024 and 20191110")
            ->groupBy('source')
            ->select(DB::raw("SUM(amount) as total, source"))->get();
        }

        return response()->json($items);
        
    }
}


