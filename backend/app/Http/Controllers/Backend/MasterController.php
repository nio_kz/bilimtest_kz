<?php

namespace App\Http\Controllers\Backend;

use App\QuizCertificate;
use App\Quizes;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use App\Http\Controllers\Controller;

class MasterController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function get_list($table, Request $request){
        $req = $request->all();

        $data = DB::table($table);
        $data = $data->where('deleted', 0);
        
        if(isset($req['where'])){
           $data = $data->whereRaw($req['where']); 
        }
        if(isset($req['filter'])){
            foreach(json_decode($req['filter']) as $key=>$value){
                $data = $data->where($key, $value); 
            }   
        }

        if($table=='users' && isset($req['q'])){
            $data = $data->whereRaw("email like '%".$req['q']."%' or first_name like '%".$req['q']."%' or last_name like '%".$req['q']."%' "); 
        }
        if($table=='konkurs_member'){
            $data = $data->orderByRaw('created_at asc'); 
        }else{
            if(isset($req['order_by'])){
                $data = $data->orderByRaw($req['order_by']); 
             }else{
                 $data = $data->orderBy('id', 'desc'); 
             }
        }
        

        if(isset($req['pagination'])&&$req['pagination']==1){
            $data = $data->paginate(isset($req['per_page']) ? $req['per_page']:25);
        }else{
            $data = $data->get();
        }


        if($table=='quiz'){
            foreach ($data as $key => $value) {
                $data[$key]->gold = $this->get_cert('gold', $value);
                $data[$key]->bronze = $this->get_cert('bronze', $value);
                $data[$key]->silver = $this->get_cert('silver', $value);
                $data[$key]->other = $this->get_cert('other', $value);
                $data[$key]->event = $this->get_cert('event', $value);
            }
        }

        

        return response()->json($data);
    }

    private function get_cert($otype, $val){
        $item = DB::table('quiz_certificate')->where('quiz_id', $val->id)->where('deleted', 0)->where('level', $otype)->first();
        return $item ? $item->certificate_id : null;
    }
    public function add_or_update($table, Request $request){
        $req = $request->all();

        $data = DB::table($table);

        if($table=='quiz'){
            $values = [
                    'name_kz'=>$req['name_kz'],
                    'name_ru'=>$req['name_ru'],
                    'duration'=>$req['duration'],
                    'amount'=>$req['amount'],
                    'teacher_id'=>$req['teacher_id'],
                    'max_questions'=>$req['max_questions'],
                    'lang'=>$req['lang'],
                    'quiz_type'=>$req['quiz_type'],
                    'quiz_for'=>$req['quiz_for'],
                    'class_id'=>$req['class_id'],
                    'lesson_id'=>$req['lesson_id'],
                    'start_at'=>$req['start_at'],
                    'end_at'=>$req['end_at'],
                    'is_active'=>$req['is_active'],
                ];
            if(isset($req['id'])){
                $id = $req['id'];
                $data = $data->where('id', $req['id'])->update($values);
                $certs = DB::table('quiz_certificate')->where('quiz_id', $id)->update(['deleted'=>1]);
            }else{
                $id = $data->insertGetId($values); 
            }
            
            foreach (['gold', 'bronze', 'silver', 'other', 'event'] as $value) {
                if(isset($req[$value])){
                    DB::table('quiz_certificate')->insert([
                        'deleted'=>0,
                        'certificate_id'=>$req[$value],
                        'level'=> $value,
                        'quiz_id' => $id
                    ]);
                }    
            }
        }else{
            unset($req['isTrusted']);
        
            if(isset($req['id'])){
                $data = $data->where('id', $req['id'])->update($req); 
            }else{
                $data->insert($req); 
            }
        }
        if($table=='invoice'){
            $invoice = DB::table($table)->where('id', $req['id'])->first();
            if($req['status']=="approved"){
                DB::table('transaction')->insert([
                    'user_id'=>$invoice->user_id,
                    'amount'=>$invoice->amount,
                    'status'=>'income',
                    'description'=>'ТҮБІРТЕК',
                    'source_id'=>$invoice->id,
                    'source'=>'invoice'
                ]);
                $this->calcUserAmount($invoice->user_id);
            }
            
        }
        return response()->json(['status'=>'success']);
    }

    public function destory(Request $request, $table){
        $req = $request->all();
        $data = DB::table($table);
        if(isset($req['id'])){
            $data = $data->where('id', $req['id'])->update(['deleted'=>1]); 
        }
        return response()->json(['status'=>'success']);
    }

    private function calcUserAmount($user_id){
        $total = DB::table("transaction")->where('user_id', $user_id)->sum("amount");
        DB::table("users")->where('id', $user_id)->update(['amount'=>$total]);
    }

}
