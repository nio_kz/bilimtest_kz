<?php

namespace App\Http\Controllers\Backend;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use App\Http\Controllers\Controller;

class CertificateController extends Controller
{

    public function certificates(Request $request){
        $certificates = DB::table("certificate")->get();
        return response()->json(['certificates'=>$certificates]);
    }

    public function upload(Request $request){
        $validator = Validator::make($request->all(), [
            'file' => 'required|max:3072000',
            
        ]);
        
        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()], 200);
        }
        $data = $request->all();
        
        $fileName = time()."-".$request->file->getClientOriginalName();

        $request->file->storeAs($request->get("folder", "certificate").'/', $fileName);
        return response()->json(['filename'=>$fileName]);
    }

    public function certificate(Request $request){
        $certificate = $request->get("certificate");
        if(isset($certificate["id"]) && $certificate["id"] > 0){
            //update
            $id = $certificate["id"];
            unset($certificate["id"]);
            DB::table("certificate")->where("id", $id)->update($certificate);
            return response()->json(['error'=>false]);
        }else{
            //insert
            DB::table("certificate")->insert($certificate);
            return response()->json(['error'=>false]);
        }
    }

    public function certificate_destroy(Request $request){
        $certificate = $request->get("certificate");
        if(isset($certificate["id"]) && $certificate["id"] > 0) {

            DB::table("certificate")->where("id", "=", $certificate["id"])->delete();
            return response()->json(['error'=>false]);
        }
        return response()->json(['error'=>true]);
    }

    public function summernote_upload(Request $request){
        $validator = Validator::make($request->all(), [
            'file' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()]);
        } else {
            $fileName = time()."-".$request->file->getClientOriginalName();
            $request->file->storeAs('summernote-uploads', $fileName);
            return '/uploads/summernote-uploads/'.$fileName;
        }
    }
}
