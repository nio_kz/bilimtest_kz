<?php

namespace App\Http\Controllers\Guest;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class OlimpiadController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth',  ['except' => [
            'o_lesson',
            'lesson_olimpiad',            
            'lesson_plans_public'
        ]]);
    }

    public function o_lesson(Request $request){
    
        $param = $request->all();
        $quiz_for = isset($param['section']) ? $param['section']:'high-school';
        $data = DB::select("select l.id, l.name, count(o.id) as total, min(o.start_at) as start_at, max(o.end_at) as end_at, max(o.duration) as duration from o_lesson l
        inner join o_olimpiad o on o.lesson_id = l.id
        where l.deleted = 0 and  o.deleted=0 and o.quiz_for='".$quiz_for."'
        group by l.id, l.name");
        return response()->json($data);
    }
    public function lesson_olimpiad(Request $request){
        $data = $request->all();
        $items = DB::table("o_olimpiad")
        ->join('o_class', 'o_class.id', '=', 'o_olimpiad.class_id')
        ->where("o_olimpiad.deleted", 0)
        ->where("o_olimpiad.lesson_id", $data['lesson'])
        ->where("o_olimpiad.quiz_for", $data['section'])
        ->select("o_olimpiad.id", "o_olimpiad.lesson_id", "o_olimpiad.class_id", "o_class.name", "o_olimpiad.amount", "o_olimpiad.quiz_for")->get();

        return response()->json($items);

    }

    public function select_olimpiad(Request $request){
        $data = $request->all();
        $item = DB::table("o_olimpiad")
        ->where("deleted", 0)
        ->where("id", $data['id'])
        ->where("class_id", $data['class_id'])
        ->where("lesson_id", $data['lesson_id'])
        ->where("quiz_for", $data['quiz_for'])->first();

        if(Auth::user()->amount < $item->amount){
            return response()->json(['status'=>'error', 'message'=>'Баланс жеткіліксіз']);
        }

        DB::beginTransaction();
            try{
            //outcome
            DB::table('transaction')->insert([
                'user_id'=>Auth::user()->id,
                'amount'=>$item->amount*-1,
                'status'=>'outcome',
                'description'=>'buy olimpiad to class:'.$item->class_id.', lesson:'.$item->lesson_id.' , olimpiad name:'.$item->name,
                'source_id'=>$item->id,
                'source'=>'olimpiad'
            ]);
            //create user quiza
            $start_at=date("Y-m-d H:i:s");

            $time = new \DateTime($start_at);
            $time->add(new \DateInterval('PT' . $item->duration . 'M'));
            $end_at = $time->format('Y-m-d H:i:s');

            $id = DB::table('o_user_olimpiad')->insertGetId([
                'cert_num' => $this->generateCertNum(),
                'is_finished'=>0,
                'start_at'=>$start_at,
                'olimpiad_id'=>$item->id,
                'user_id'=>isset($data['user_id']) ? $data['user_id']:Auth::user()->id,
            ]);
            DB::table('o_user_olimpiad')->where('id', $id)->update(['token'=>md5($id)]);
            // questions
            $random_questions = DB::table("o_random_question")
            ->where("class_id", $item->class_id)
            ->where("lesson_id", $item->lesson_id)
            ->inRandomOrder()
            ->limit($item->max_question)
            ->get();
            if(count($random_questions)>=$item->max_question){
                foreach($random_questions as  $rq){
                    $uo_q = DB::table("o_user_olimpiad_question")->insertGetId([
                        'olimpiad_id'=>$id,
                        'question_id'=>$rq->question_id
                    ]);
                    $answers = DB::table('o_question_answer')
                    ->where('question_id', $rq->question_id)
                    ->where('deleted', 0)
                    ->inRandomOrder()
                    ->get();
                    foreach($answers as $answer){
                        DB::table("o_user_olimpiad_q_answer")
                        ->insert([
                            'user_olimpiad_question_id'=>$uo_q,
                            'answer_id'=> $answer->id,
                            'col' => $answer->col,
                            'correct_answer_id'=>$answer->correct_id,
                            'point'=>$answer->point

                        ]);
                    }
                    
                }
            }else{
                $qs = DB::table("o_question")
                ->where("class_id", $item->class_id)
                ->where("lesson_id", $item->lesson_id)
                ->where("deleted", 0)
                ->inRandomOrder()
                ->limit($item->max_question)
                ->get();
                foreach($qs as  $q){
                    $uo_q = DB::table("o_user_olimpiad_question")->insertGetId([
                        'olimpiad_id'=>$id,
                        'question_id'=>$q->id
                    ]);
                    $answers = DB::table('o_question_answer')
                    ->where('question_id', $q->id)
                    ->where('deleted', 0)
                    ->inRandomOrder()
                    ->get();
                    foreach($answers as $answer){
                        DB::table("o_user_olimpiad_q_answer")
                        ->insert([
                            'user_olimpiad_question_id'=>$uo_q,
                            'answer_id'=> $answer->id,
                            'col' => $answer->col,
                            'correct_answer_id'=>$answer->correct_id,
                            'point'=>$answer->point

                        ]);
                    }
                    
                }
            }
            
            // answers
            $this->calcUserAmount(Auth::user()->id);
            // return token
            DB::commit();
            return response()->json(['status'=>'success', 'token'=>md5($id)]);
            }catch(\Exception $e){
                return response()->json(['status'=>'error', 'message'=>$e->getMessage()]);
                DB::rollBack();
            }
        //end 


    }

    public function start(Request $request){
        $token = $request->get("token");
        $ouo = DB::table("o_user_olimpiad")->where('token', $token)->first();
        
        if(!$ouo){
            return response()->json(['status'=>'error', 'message'=>'Олимпиада табылмады']);
        }

        if($ouo->is_finished){
            $this->calculate_u_0($token);
            return response()->json(['status'=>'finished', 'message'=>'Құрметті қолданшы тапсырмаларды орындау уақыты аяқталған. Нәтижесін көру үшін <a href="/user/result">осында</a> басыңыз.']);
        }
        $olimpiad = DB::table("o_olimpiad")->where('id', $ouo->olimpiad_id)->first();
        $lesson = DB::table("o_lesson")->where('id', $olimpiad->lesson_id)->first();
        $class = DB::table("o_class")->where('id', $olimpiad->class_id)->first();

        $to_time = strtotime(date("Y-m-d H:m:s"));
        $from_time = strtotime($ouo->end_at);
        
        if(strtotime(date("Y-m-d H:i:s"))-strtotime($ouo->start_at)>$olimpiad->duration*60){
            
            DB::table("o_user_olimpiad")->where('token', $token)->update([
                'is_finished'=>1
            ]);
            
            return response()->json(['status'=>'expired', 'message'=>'Құрметті қолданшы тапсырмаларды орындау уақыты аяқталған. Нәтижесін көру үшін <a href="/user/result">осында</a> басыңыз.']);
        }

        $questions = DB::table("o_user_olimpiad_question")
        ->join("o_user_olimpiad_q_answer", "o_user_olimpiad_q_answer.user_olimpiad_question_id", "=", "o_user_olimpiad_question.id")
        ->where('o_user_olimpiad_question.olimpiad_id', $ouo->id)
        ->groupBy("o_user_olimpiad_question.question_id")
        ->select(DB::raw("o_user_olimpiad_question.question_id, sum(o_user_olimpiad_q_answer.checked) as is_checked"))
        ->get();

        $seconds = strtotime($ouo->start_at)-strtotime(date("Y-m-d H:i:s"));
        $seconds = ($olimpiad->duration*60) + $seconds;
        return response()->json(['status'=>'success', 'timer'=>$seconds, 'items'=>$questions, 'olimpiad'=>[
            'olesson'=>$lesson ? $lesson->name:"---",
            'oclass'=>$class ? $class->name:"---",
            'oid'=>$olimpiad->id
        ]]);

    }

    public function question(Request $request){
        $token = $request->get("token");
        $qid = $request->get("question");
        $ouo = DB::table("o_user_olimpiad")->where('token', $token)->first();
        $item = DB::table("o_user_olimpiad_question")->where('olimpiad_id', $ouo->id)->where('question_id', $qid)->first();
        $q = DB::table("o_question")->where('id', $item->question_id)->where('deleted', 0)->first();
        $answers = DB::table("o_user_olimpiad_q_answer")
        ->join("o_question_answer", "o_question_answer.id", '=', "o_user_olimpiad_q_answer.answer_id")
        ->where('o_user_olimpiad_q_answer.user_olimpiad_question_id', $item->id)
        ->select("o_user_olimpiad_q_answer.id","o_user_olimpiad_q_answer.col","o_user_olimpiad_q_answer.answer_id" ,"o_user_olimpiad_q_answer.checked","o_user_olimpiad_q_answer.correct_answer_id", "o_question_answer.name")
        ->get();
        return response()->json(['question'=>$q, 'answer'=>$answers]);

    }

    public function question_update(Request $request){
        $items = $request->all();
        foreach($items as $item){
            DB::table("o_user_olimpiad_q_answer")->where("id", $item['id'])->update([
                'checked'=>$item['checked'],
                'correct_answer_id'=>$item['correct_answer_id']
            ]);
        }
        return response()->json(['status'=>'success']);
    }

    public function olimpiad_finish(Request $request){
        $data = $request->all();
        DB::table("o_user_olimpiad")->where('token', $data['id'])->update([
            'is_finished'=>1,
            'end_at'=>date("Y-m-d H:i:s.u")
            ]);
        $this->calculate_u_0($data['id']);
        return response()->json(['status'=>'success']);

    }
    private function generateCertNum()
    {
        $cert_num = DB::table('o_user_olimpiad')->where('cert_num', 'like' ,'A%')->max('cert_num');

        if ($cert_num) {
            $num = intval(str_replace("A-", "", $cert_num));
            $num = $num + 1;
            return 'A-' . (str_pad($num, 7, '0', STR_PAD_LEFT));

        } else {
            return 'A-0000001';
        }

    }
    private function calcUserAmount($user_id){
        $total = DB::table("transaction")->where('user_id', $user_id)->whereIn('status', ['income', 'outcome'])->sum("amount");
        DB::table("users")->where('id', $user_id)->update(['amount'=>$total]);
    }

    
    private function calculate_u_0($token)
    {
       $u_o = DB::table('o_user_olimpiad')->where('token', $token)->first();
       if($u_o){
           $items = DB::table("o_user_olimpiad_question")->where('olimpiad_id', $u_o->id)->get();
           $max_point = $point = 0;
           foreach($items as $item){
                $_q = DB::table("o_question")->where('id', $item->question_id)->first();
                $qs = DB::table("o_user_olimpiad_q_answer")->where('user_olimpiad_question_id', $item->id)->get();
                if($_q->q_type=="cross"){
                    foreach($qs as $q){
                        if($q->col==1){
                            foreach($qs as $qq){
                                if($qq->col==2){
                                    if($q->correct_answer_id == $qq->answer_id){
                                        if($qq->correct_answer_id == $q->answer_id){
                                            $as = DB::table("o_question_answer")->where('id', $q->answer_id)->first();
                                            $point = $point + $as->point;          
                                        }
                                    }
                                }
                            }
                        }
                    }
                }else{
                    foreach($qs as $q){
                        $as = DB::table("o_question_answer")->where('id', $q->answer_id)->first();
                        $max_point = $max_point + $as->point;
                        if($q->checked==1){
                            $point = $point + $as->point;
                        }
                    }
                }
           }
           DB::table('o_user_olimpiad')->where('token', $token)->update(['max_point'=>$max_point, 'point'=>$point]);
       }

    }

}