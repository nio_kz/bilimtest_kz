<?php


namespace App\Http\Controllers\Guest;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Anam\PhantomMagick\Converter;
use App\Models\User;
use Endroid\QrCode\QrCode;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => [
            'certificate', 'certificate_html', 'certificate_gt_html', 'certificate_gramota_thanks', 'konkurs_certificate', 'konkurs_certificate_html', 'callback_pult24'
        ]]);
    }
    public function unfinished()
    {
        $items = DB::select("SELECT DISTINCT oo.name, ouo.token, ouo.is_finished, ouo.user_id FROM o_user_olimpiad ouo
        inner join o_olimpiad oo on oo.id=ouo.olimpiad_id
        
        where ouo.is_finished=0 and ouo.user_id=" . Auth::user()->id ."");
        return response()->json($items);
    }
    public function transaction(Request $request)
    {
        return response()->json(DB::table('transaction')->where('user_id', Auth::user()->id)->orderBy("created_at", "desc")->paginate(20));
    }
    public function transfer(Request $request)
    {
        return response()->json(DB::table('transaction')->where('user_id', Auth::user()->id)->where('source', 'transfer')->orderBy("created_at", "desc")->paginate(20));
    }
    public function profile_post(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'email' => 'required|email',
            'user_type' => 'required',
            'gender' => 'required',
            'school_id' => 'required',

        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 200);
        }
        $user_thumb = Auth::user()->thumbnail;
        if ($request->hasFile('file')) {
            $fileName = time() . "-" . $request->file->getClientOriginalName();
            $request->file->storeAs($request->get("folder", "user_avatar") . '/', $fileName);
            $user_thumb = '/uploads/' . $request->get("folder", "user_avatar") . '/' . $fileName;
        }



        $data = $request->all();
        unset($data['file']);
        $data['thumbnail'] = $user_thumb;
        DB::table("users")->where("id", Auth::user()->id)->update($data);

        return response()->json(['status' => 'success', 'message' => 'Жеке мәліметтер сәтті өзгертілді ']);
    }
    public function transfer_post(Request $request)
    {
        $data = $request->all();
        $user = DB::table("users")->where("deleted", 0)->where("unumber", $data['unumber'])->first();

        if ($user && Auth::user()->amount >= $data['amount']) {

            DB::beginTransaction();
            try {
                DB::table('transaction')->insert([
                    'user_id' => Auth::user()->id,
                    'amount' => $data['amount'] * -1,
                    'status' => 'outcome',
                    'description' => 'transfer to ' . $user->first_name . ' ' . $user->last_name . ' , unumber:' . $user->unumber,
                    'source_id' => Auth::user()->unumber,
                    'source' => 'transfer'
                ]);
                DB::table('transaction')->insert([
                    'user_id' => $user->id,
                    'amount' => $data['amount'],
                    'status' => 'income',
                    'description' => 'from ' . Auth::user()->first_name . ' ' . Auth::user()->last_name . ' , unumber:' . Auth::user()->unumber,
                    'source_id' => Auth::user()->unumber,
                    'source' => 'transfer'
                ]);
                DB::commit();
                $this->calcUserAmount(Auth::user()->id);
                $this->calcUserAmount($user->id);
                return response()->json(["status" => "success"]);
            } catch (\Exception $e) {
                var_dump($e->getMessage());
                DB::rollBack();
            }
        }
        if (!$user) {
            return response()->json(["status" => "error", "message" => "Қолданушы табылмады"]);
        }
        if (Auth::user()->amount < $data['amount']) {
            return response()->json(["status" => "error", "message" => "Баланс жеткіліксіз"]);
        }
    }


    public function invoice(Request $request)
    {
        return response()->json(DB::table('invoice')->where('user_id', Auth::user()->id)->where('deleted', '0')->orderBy("created_at", "desc")->paginate(20));
    }

    public function invoice_post(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'file' => 'required|max:3072000',
            'amount' => 'required'

        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 200);
        }
        $data = $request->all();

        $fileName = time() . "-" . $request->file->getClientOriginalName();

        $request->file->storeAs($request->get("folder", "invoice") . '/', $fileName);

        DB::table("invoice")->insert([
            'user_id' => Auth::user()->id,
            'invoice_path' => "invoice/" . $fileName,
            'amount' => $data['amount'],
            'status' => 'pending',
            'deleted' => 0
        ]);
        return response()->json(['status' => "success", "message" => 'sent success']);
    }
    public function invoice_destory(Request $request)
    {
        $data = $request->all();
        DB::table("invoice")->where('id', $data['id'])->update(['deleted' => 0]);

        return response()->json(['status' => 'success']);
    }

    public function certificates()
    {
        $data = DB::table("o_user_olimpiad")->where("user_id", Auth::user()->id)
            ->where("is_finished", 1)
            ->select(DB::raw("token, cert_num,  CAST((100*point/max_point) as UNSIGNED) as point"))
            ->get();
        return response()->json($data);
    }
    public function konkurs_certificate($type, $id, $pos, Request $request)
    {
        $download_type = $request->get("download_type", null);

        try {
            $konkurs_member = DB::table("konkurs_member")->where("id", $id)->first();

            $konkurs = DB::table("konkurs")->where("id", $konkurs_member->konkurs_id)->first();

            $cert_id = $konkurs->certificate_id;

            if ($pos == 1) {
                $cert_id = $konkurs->pos1;
            }
            if ($pos == 2) {
                $cert_id = $konkurs->pos2;
            }
            if ($pos == 3) {
                $cert_id = $konkurs->pos3;
            }
            $certificate = DB::table("certificate")->where("id", $cert_id)->first();

            $c = new Converter();

            $c->source("http://api.megatalant.kz/api/user/konkurs-certificate-html/" . $id . "/" . $pos);
            if ($type == "image") {
                $c->toJpg();
                if ($certificate->orientation == 'landscape') {
                    $c->width(1122);
                    $c->height(793);
                } else {
                    $c->width(793);
                    $c->height(1122);
                }
            }
            if ($type == "pdf") {
                $w = $h = 0;
                if ($certificate->orientation == 'landscape') {
                    $w = 1122;
                    $h = 793;
                } else {
                    $w = 793;
                    $h = 1122;
                }
                $options = [
                    'width' => $w . 'px',
                    'height' => ($h + 5) . 'px',
                    'zoomfactor' => 1,
                    'orientation' => $certificate->orientation,
                    'margin' => '0'
                ];
                $c->toPDF($options);
            }
        } catch (\Exception $e) {
            return $e->getMessage();
        }
        try {
            $content = "result";
            $c->quality(100);
            if ($type == "image") {
                $c->save(public_path() . '/uploads/tmp/' . $id . '.jpg');
                $content = file_get_contents(public_path() . '/uploads/tmp/' . $id . '.jpg');
                if (file_exists(public_path() . '/uploads/tmp/' . $id . '.jpg')) {
                    unlink(public_path() . '/uploads/tmp/' . $id . '.jpg');
                }
            }
            if ($type == "pdf") {
                $c->save(public_path() . '/uploads/tmp/' . $id . '.pdf');
                $content = file_get_contents(public_path() . '/uploads/tmp/' . $id . '.pdf');

                if (file_exists(public_path() . '/uploads/tmp/' . $id . '.pdf')) {
                    unlink(public_path() . '/uploads/tmp/' . $id . '.pdf');
                }
            }
            if ($download_type == "download") {

                return response($content, 200, [
                    'Content-Type' => $type == "pdf" ? 'application/pdf' : 'image/jpeg',
                    'Content-Disposition' => 'attachment; filename=' . str_replace(",", " ", $konkurs_member->full_name . "." . ($type == "pdf" ? "pdf" : "jpg")),
                ]);
            }
        } catch (\Exception $x) {
            var_dump($x->getMessage());
        }

        return $content;
    }

    public function konkurs_certificate_html($id, $pos)
    {
        $konkurs_member = DB::table("konkurs_member")->where("id", $id)->first();

        $konkurs = DB::table("konkurs")->where("id", $konkurs_member->konkurs_id)->first();

        $cert_id = $konkurs->certificate_id;

        if ($pos == 1) {
            $cert_id = $konkurs->pos1;
        }
        if ($pos == 2) {
            $cert_id = $konkurs->pos2;
        }
        if ($pos == 3) {
            $cert_id = $konkurs->pos3;
        }
        $user = DB::table("users")->where("id", $konkurs_member->user_id)->first();
        $s = DB::table('school')->where('id', $user->school_id)->first();
        $r = DB::table('location')->where('id', $s->location_id)->first();
        $c = DB::table('location')->where('id', $r->parent_id)->first();
        // var_dump($s, $r, $c);
        $certificate = DB::table("certificate")->where("id", $cert_id)->first();
        $content = $certificate->content;
        $content = str_replace(":certificate_number", date("y") . '/000' . $konkurs_member->id,  $content);
        $date = date("d.m.Y", strtotime($konkurs_member->created_at));
        $content = str_replace(":date", $date,  $content);
        $content = str_replace(":name", $konkurs_member->full_name,  $content);

        $content = str_replace(":konkurs_name", "<b>" . $konkurs->title . "</b>",  $content);
        $teacher_name = "";
        if ($user->user_type == "high_school_student") {
            if ($user->teacher_unumber) {
                $t = DB::table('users')->where('unumber', $user->teacher_unumber)->first();
                if ($t) {
                    $teacher_name = $t->middle_name . ' ' . $t->last_name . ' ' . $t->first_name;
                }
            } else {
                $teacher_name = $user->teacher_name;
            }
        }

        $content = str_replace(":user_type", $user->user_type == 'teacher' ? 'мұғалімі' : ($user->user_type == 'student' ? 'студенті' : 'оқушысы'),  $content);
        $content = str_replace(":teacher_name", ($teacher_name != "" ? "Жетекшісі: " . $teacher_name : ""),  $content);


        $content = str_replace(":school", $c->name . ', ' . $r->name . ', ' . $s->name,  $content);
        $qrCode = new QrCode(date("y") . '/000' . $konkurs_member->id . ', ' . $user->first_name . ' ' . $user->last_name . ', ' . $c->name . ', ' . $r->name . ', ' . $s->name);
        $qrCode->setSize(130);
        $data = $qrCode->writeDataUri();
        $content = str_replace(":qrcode", $data,  $content);

        return view("certificate_template", ['certificate' => $certificate, 'content' => $content]);
    }
    private function get_certificates($year, $user_id)
    {
        return DB::table("o_user_olimpiad")->where("user_id", $user_id)
            ->where("is_finished", 1)
            ->whereRaw("YEAR(start_at)=" . $year)
            ->select(DB::raw("token, cert_num,  CAST((100*point/max_point) as UNSIGNED) as point"))
            ->get();
    }
    public function my_result_year()
    {

        $user_id = Auth::user()->id;

        $items = DB::select("SELECT oo.id,oo.cert_num,  oo.token, ol.name as lesson, oo.max_point, oo.point, oc.name as class, DATE_FORMAT(oo.start_at, '%Y-%m-%d') as start_at, TIMESTAMPDIFF(SECOND,oo.start_at,oo.end_at) as total_seconds
        FROM `o_user_olimpiad` oo
        inner join o_olimpiad o_o on  o_o.id = oo.olimpiad_id
        inner join o_lesson ol ON ol.id = o_o.lesson_id
        inner join o_class oc on oc.id = o_o.class_id
        WHERE user_id=" . $user_id . "  Order BY oo.start_at desc");
        $data = [];
        foreach ($items as $item) {
            $data[] = [
                'id' => $item->id,
                'token' => $item->token,
                'cert_num' => $item->cert_num,
                'lesson' => $item->lesson,
                'class' => $item->class,
                'start_at' => $item->start_at,
                'total_seconds' => gmdate("H:m:s", $item->total_seconds),
                'total' => $item->max_point,
                'point' => $item->point

            ];
        }
        return response()->json($data);
    }
    public function certificate_html($token, $user_token)
    {
        $ouo = DB::table('o_user_olimpiad')->where('token', $token)->first();
        $user = DB::table('users')->where('id', $ouo->user_id)->first();
        $o = DB::table('o_olimpiad')->where('id', $ouo->olimpiad_id)->first();
        $l = DB::table('o_lesson')->where('id', $o->lesson_id)->first();
        $s = DB::table('school')->where('id', $user->school_id)->first();
        $r = DB::table('location')->where('id', $s->location_id)->first();
        $c = DB::table('location')->where('id', $r->parent_id)->first();

        // max_point -- 100
        // point -- x  x=100*point/max_point
        $cert_id = 0;
        if (intval((100 * $ouo->point) / $ouo->max_point) >= 90) {
            $cert_id = $o->gold_cert_id;
        }

        if (intval((100 * $ouo->point) / $ouo->max_point) >= 80 && intval((100 * $ouo->point) / $ouo->max_point) < 90) {
            $cert_id = $o->silver_cert_id;
        }

        if (intval((100 * $ouo->point) / $ouo->max_point) >= 70 && intval((100 * $ouo->point) / $ouo->max_point) < 80) {
            $cert_id = $o->bronze_cert_id;
        }

        if (intval((100 * $ouo->point) / $ouo->max_point) < 70) {
            $cert_id = $o->other_cert_id;
        }


        $certificate = DB::table("certificate")->where("id", $cert_id)->first();
        $content = $certificate->content;
        $content = str_replace(":certificate_number", $ouo->cert_num,  $content);
        $date = date("d.m.Y", strtotime($ouo->start_at));
        $content = str_replace(":date", $date,  $content);
        $content = str_replace(":name", $user->middle_name . ' ' . $user->last_name . ' ' . $user->first_name,  $content);
        $teacher_name = "";
        if ($user->user_type == "high_school_student") {
            if ($user->teacher_unumber) {
                $t = DB::table('users')->where('unumber', $user->teacher_unumber)->first();
                if ($t) {
                    $teacher_name = $t->middle_name . ' ' . $t->last_name . ' ' . $t->first_name;
                }
            } else {
                $teacher_name = $user->teacher_name;
            }
        }

        $content = str_replace(":teacher_name", ($teacher_name != "" ? "Жетекшісі: " . $teacher_name : ""),  $content);
        $content = str_replace(":lesson", $l->name,  $content);
        $content = str_replace(":quiz_for", $user->user_type == 'teacher' ? 'мұғалімі' : ($user->user_type == 'student' ? 'студенті' : 'оқушысы'),  $content);
        $content = str_replace(":school", $c->name . ', ' . $r->name . ', ' . $s->name,  $content);
        $qrCode = new QrCode($ouo->cert_num . ', ' . $user->first_name . ' ' . $user->last_name . ', ' . $c->name . ', ' . $r->name . ', ' . $s->name);
        $qrCode->setSize(130);
        $data = $qrCode->writeDataUri();
        $content = str_replace(":qrcode", $data,  $content);
        return view("certificate_template", ['certificate' => $certificate, 'content' => $content]);
    }
    public function certificate($type, $token)
    {
        // var_dump(); die;
        try {
            $ouo = DB::table('o_user_olimpiad')->where('token', $token)->first(); //->where('user_id', Auth::user()->id)->first();
            $o = DB::table('o_olimpiad')->where('id', $ouo->olimpiad_id)->first();

            $cert_id = 0;
            if (intval((100 * $ouo->point) / $ouo->max_point) >= 90) {
                $cert_id = $o->gold_cert_id;
            }

            if (intval((100 * $ouo->point) / $ouo->max_point) >= 80 && intval((100 * $ouo->point) / $ouo->max_point) <= 90) {
                $cert_id = $o->silver_cert_id;
            }

            if (intval((100 * $ouo->point) / $ouo->max_point) >= 70 && intval((100 * $ouo->point) / $ouo->max_point) <= 80) {
                $cert_id = $o->bronze_cert_id;
            }

            if (intval((100 * $ouo->point) / $ouo->max_point) < 70) {
                $cert_id = $o->bronze_cert_id;
            }


            $certificate = DB::table("certificate")->where("id", $cert_id)->first();

            $c = new Converter();
            $c->source("http://api.megatalant.kz/api/user/certificate-html/" . $token . "/" . md5($ouo->user_id));
            if ($type == "image") {
                $c->toJpg();
                if ($certificate->orientation == 'landscape') {
                    $c->width(1122);
                    $c->height(793);
                } else {
                    $c->width(793);
                    $c->height(1122);
                }
            }
            if ($type == "pdf") {
                $w = $h = 0;
                if ($certificate->orientation == 'landscape') {
                    $w = 1122;
                    $h = 793;
                } else {
                    $w = 793;
                    $h = 1122;
                }
                $options = [
                    'width' => $w . 'px',
                    'height' => ($h + 5) . 'px',
                    'zoomfactor' => 1,
                    'orientation' => $certificate->orientation,
                    'margin' => '0'
                ];
                $c->toPDF($options);
            }
        } catch (\Exception $e) {
            var_dump($e->getMessage());
        }

        $c->quality(100);
        if ($type == "image") {

            $c->save(public_path() . '/uploads/tmp/' . $token . '.jpg');
            $content = file_get_contents(public_path() . '/uploads/tmp/' . $token . '.jpg');
            if (file_exists(public_path() . '/uploads/tmp/' . $token . '.jpg')) {
                unlink(public_path() . '/uploads/tmp/' . $token . '.jpg');
            }
        }
        if ($type == "pdf") {
            $c->save(public_path() . '/uploads/tmp/' . $token . '.pdf');
            $content = file_get_contents(public_path() . '/uploads/tmp/' . $token . '.pdf');
            if (file_exists(public_path() . '/uploads/tmp/' . $token . '.pdf')) {
                unlink(public_path() . '/uploads/tmp/' . $token . '.pdf');
            }
        }

        return response($content, 200, [
            'Content-Type' => $type == "pdf" ? 'application/pdf' : 'image/jpeg'
        ]);
    }
    public function result($token)
    {
        $data = [];
        $ouo = DB::table('o_user_olimpiad')->where('token', $token)->first();
        if ($ouo) {
            $olimpiad = DB::table("o_olimpiad")->where('id', $ouo->olimpiad_id)->first();
            $lesson = DB::table("o_lesson")->where('id', $olimpiad->lesson_id)->first();
            $class = DB::table("o_class")->where('id', $olimpiad->class_id)->first();
            $data = [
                'max_point' => $ouo->max_point,
                'point' => $ouo->point,
                'lesson' => $lesson->name,
                'class' => $class->name,
                'questions' => $this->get_questions($ouo->id)
            ];
        }
        return response()->json($data);
    }

    private function get_questions($id)
    {
        $questions = [];
        $items = DB::table("o_question")
            ->join('o_user_olimpiad_question', 'o_user_olimpiad_question.question_id', '=', 'o_question.id')
            ->where("o_user_olimpiad_question.olimpiad_id", $id)
            ->select("o_user_olimpiad_question.id", "o_question.name", "o_question.max_choice", "o_question.q_type")->get();
        foreach ($items as $item) {
            $answers = DB::table("o_user_olimpiad_q_answer")
                ->join("o_question_answer", "o_question_answer.id", '=', "o_user_olimpiad_q_answer.answer_id")
                ->where('o_user_olimpiad_q_answer.user_olimpiad_question_id', $item->id)
                ->select(
                    "o_user_olimpiad_q_answer.id",
                    "o_user_olimpiad_q_answer.answer_id",
                    "o_user_olimpiad_q_answer.col",
                    "o_user_olimpiad_q_answer.checked",
                    "o_user_olimpiad_q_answer.correct_answer_id",
                    "o_question_answer.name",
                    DB::raw("IF(o_question_answer.point>0, true, false) as is_true"),
                    DB::raw("IF(o_question_answer.correct_id = o_user_olimpiad_q_answer.correct_answer_id, true, false) as is_true_cross")
                )
                ->get();
            $questions[] = [
                'id' => $item->id,
                'name' => $item->name,
                'q_type' => $item->q_type,
                'max_choice' => $item->max_choice,
                'answers' => $answers
            ];
        }
        return $questions;
    }
    public function me(Request $request)
    {
        $user = Auth::user();
        return response()->json(['user' => $user]);
    }

    public function user(Request $request)
    {
        $user = Auth::user();
        return response()->json($user);
    }

    public function user_school(Request $request)
    {
        $school_id = Auth::user()->school_id;
        $school = DB::table("school")->where("id", $school_id)->first();
        $schools = DB::table("school")->where("location_id", $school->location_id)->get();
        $city_id = $school->location_id;
        $city = DB::table("location")->where("id", $city_id)->first();
        $cities = DB::table("location")->where("parent_id", $city->parent_id)->get();
        $region_id =  $city->parent_id;

        return response()->json([
            'school_id' => $school_id,
            'schools' => $schools,
            'city_id' => $city_id,
            'cities' => $cities,
            'region_id' => $region_id
        ]);
    }

    private function calcUserAmount($user_id)
    {
        $total = DB::table("transaction")->where('user_id', $user_id)->whereIn('status', ['income', 'outcome'])->sum("amount");
        DB::table("users")->where('id', $user_id)->update(['amount' => $total]);
    }

    public function summernote_upload(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'file' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()]);
        } else {
            $fileName = time() . "-" . $request->file->getClientOriginalName();
            $request->file->storeAs('summernote-uploads', $fileName);
            return '/uploads/summernote-uploads/' . $fileName;
        }
    }


    public function kaspi_online(Request $request)
    {
        $amount = intval($request->get("amount", 0));
        if ($amount <= 0) {
            return response()->json(['status' => 'error', 'message' => 'Сіз қажетті соманы енгізбедіңіз!']);
        }
        $tran_id = strtoupper(time());
        DB::table('transaction')->insertGetId([
            'user_id' => Auth::user()->id,
            'amount' => $amount,
            'status' => 'pending',
            'description' => 'init kaspi bank online',
            'source_id' => $tran_id,
            'source' => 'kaspi-online'
        ]);

        $param = array();
        $param['transaction'] = $tran_id;
        $param['unumber'] = Auth::user()->unumber;
        $param['amount'] = number_format($amount, 2);
        $param['returnUrl'] = ('https://www.edulife.kz/kasp/result?tranid=' . $tran_id);

        return response()->json(['status' => 'success', 'kaspi' => $param]);
    }
    public function paybox(Request $request)
    {
        date_default_timezone_set("Asia/Aqtobe");
        $amount = $request->get("amount", 0);
        $user = Auth::user();
        $dateTime = new \DateTime();
        $dateTime->modify('+5 minutes');
        $data = [
            "order" => 'order-' . date("Y-m-d H:i:s"),
            "amount" => $amount,
            "currency" => "KZT",
            "description" => "Charge account use patbox",
            "expires_at" => $dateTime->format("Y-m-d H:i:s"),
            "cleared" => true,
            "options" => [
                "callbacks" => [
                    "result_url" => "http://localhost:3000/paybox/result"
                ]
            ]
        ];
        $data_string = json_encode($data);
        $url = "https://api.paybox.money/v4/payments";
        try {
            //setting the curl parameters.
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_ENCODING, "");
            curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
            curl_setopt($ch, CURLOPT_TIMEOUT, 30);
            curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                "Cache-Control: no-cache",
                'Content-Length: ' . strlen($data_string),
                'Authorization: Basic ' . base64_encode("520825:5w88HnALP9A0pyjO"),
                "Idempotency-Key: " . $this->uuid()
            ));
            //return response('order-'.date("Y-m-d H:i:s").date_default_timezone_get());
            $data = curl_exec($ch);
            curl_close($ch);
            $data_array = json_decode($data);
            DB::table('transaction')->insert([
                'user_id' => Auth::user()->id,
                'amount' => $amount,
                'status' => 'pending',
                'description' => $data,
                'source_id' => $data_array->id,
                'source' => 'paybox'
            ]);
            return response()->json(['status' => 'success', 'result' => $data_array]);
        } catch (\Exception $ex) {
            return response()->json($ex->getMessage());
        }
    }

    public function gramota_list()
    {
        $items = DB::table("gramota_thanks")->where('user_id', Auth::user()->id)->where('is_gramota', 1)->get();
        return response()->json($items);
    }

    public function thanks_list()
    {
        $items = DB::table("gramota_thanks")->where('user_id', Auth::user()->id)->where('is_gramota', 0)->get();
        return response()->json($items);
    }

    public function gramota_store(Request $request)
    {
        $data = $request->get("data");
        $return_data = [
            "number1" => "",
            "number2" => "",
            "number3" => "",
            "number4" => "",
            "number5" => ""
        ];
        $is_confirm = true;

        foreach ($return_data as $key => $value) {
            if (!isset($data[$key])) {
                $is_confirm = false;
                $return_data[$key] = "ID енгізіңіз";
            }
        }
        $_uq = array_unique($data);
        foreach ($data as $key => $value) {
            if (!isset($_uq[$key])) {
                $is_confirm = false;
                $return_data[$key] = "Бұл ID енгізілген";
            } else if (trim($_uq[$key]) == "") {
                $is_confirm = false;
                $return_data[$key] = "ID енгізіңіз";
            } else {
                $result = $this->checkUserGramota($value, 80);
                if ($result != "true") {
                    $is_confirm = false;
                    $return_data[$key] = $result;
                }
            }
        }


        if (!$is_confirm) {
            return response()->json(["status" => "error", "data" => $return_data]);
        } else {
            // save gramota
            $cert = DB::table("certificate")->where('deleted', 0)->where('certificate_type', 'gramota')->first();
            $id = DB::table("gramota_thanks")->insertGetId([
                'user_id' => Auth::user()->id,
                'cert_num' => $this->generateGramotaCertNum(),
                'author_name' => Auth::user()->first_name . " " . Auth::user()->last_name,
                'is_gramota' => 1,
                'cert_id' => $cert->id
            ]);
            foreach ($data as $r) {
                DB::table("gramota_thanks_users")->insert([
                    'unumber' => $r,
                    'gramota_thanks' => $id,
                    'is_gramota' => 1
                ]);
            }
        }
        return response()->json(["status" => "success"]);
    }

    public function thanks_store(Request $request)
    {
        $data = $request->get("data");
        $return_data = [
            "number1" => "",
            "number2" => "",
            "number3" => "",
            "number4" => "",
            "number5" => ""
        ];
        $is_confirm = true;
        foreach ($return_data as $key => $value) {
            if (!isset($data[$key])) {
                $is_confirm = false;
                $return_data[$key] = "ID енгізіңіз";
            }
        }

        $_uq = array_unique($data);
        foreach ($data as $key => $value) {
            if (!isset($_uq[$key])) {
                $is_confirm = false;
                $return_data[$key] = "Бұл ID енгізілген";
            } else if (trim($_uq[$key]) == "") {
                $is_confirm = false;
                $return_data[$key] = "ID енгізіңіз";
            } else {
                $result = $this->checkUserGramota($value, 0);
                if ($result != "true") {
                    $is_confirm = false;
                    $return_data[$key] = $result;
                }
            }
        }


        if (!$is_confirm) {
            return response()->json(["status" => "error", "data" => $return_data]);
        } else {
            // save gramota
            $cert = DB::table("certificate")->where('deleted', 0)->where('certificate_type', 'thanks')->first();
            $id = DB::table("gramota_thanks")->insertGetId([
                'user_id' => Auth::user()->id,
                'cert_num' => $this->generateGramotaCertNum(),
                'author_name' => Auth::user()->first_name . " " . Auth::user()->last_name,
                'is_gramota' => 0,
                'cert_id' => $cert->id
            ]);
            foreach ($data as $r) {
                DB::table("gramota_thanks_users")->insert([
                    'unumber' => $r,
                    'gramota_thanks' => $id,
                    'is_gramota' => 0
                ]);
            }
        }
        return response()->json(["status" => "success"]);
    }

    public function certificate_gramota_thanks($type, $id)
    {

        try {
            $gt = DB::table('gramota_thanks')->where('id', $id)->first(); //->where('user_id', Auth::user()->id)->first();


            $cert_id = $gt->cert_id;


            $certificate = DB::table("certificate")->where("id", $cert_id)->first();

            $c = new Converter();
            // var_dump("https://admin.edulife.kz/api/user/certificate-gt-html/".$id."/".$gt->user_id); die;
            $c->source("http://api.megatalant.kz/api/user/certificate-gt-html/" . $id . "/" . $gt->user_id);
            // $c->source("https://google.com");
            if ($type == "image") {
                $c->toJpg();
                if ($certificate->orientation == 'landscape') {
                    $c->width(1122);
                    $c->height(793);
                } else {
                    $c->width(793);
                    $c->height(1122);
                }
            }

            if ($type == "pdf") {
                $w = $h = 0;
                if ($certificate->orientation == 'landscape') {
                    $w = 1122;
                    $h = 793;
                } else {
                    $w = 793;
                    $h = 1122;
                }
                $options = [
                    'width' => $w . 'px',
                    'height' => ($h + 5) . 'px',
                    'zoomfactor' => 1,
                    'orientation' => $certificate->orientation,
                    'margin' => '0'
                ];
                $c->toPDF($options);
            }
        } catch (\Exception $e) {
            var_dump($e->getMessage());
        }

        $c->quality(100);
        if ($type == "image") {

            try {
                $c->save(public_path() . '/uploads/tmp/' . $gt->cert_num . '.jpg');
                $content = file_get_contents(public_path() . '/uploads/tmp/' . $gt->cert_num . '.jpg');
            } catch (\Exception $e) {
                var_dump($e->getMessage());
                die;
            }



            if (file_exists(public_path() . '/uploads/tmp/' . $gt->cert_num . '.jpg')) {
                unlink(public_path() . '/uploads/tmp/' . $gt->cert_num . '.jpg');
            }
        }

        if ($type == "pdf") {
            $c->save(public_path() . '/uploads/tmp/' . $gt->cert_num . '.pdf');
            $content = file_get_contents(public_path() . '/uploads/tmp/' . $gt->cert_num . '.pdf');
            if (file_exists(public_path() . '/uploads/tmp/' . $gt->cert_num . '.pdf')) {
                unlink(public_path() . '/uploads/tmp/' . $gt->cert_num . '.pdf');
            }
        }

        return $content;
    }
    public function certificate_gt_html($id, $user_id)
    {

        $gt = DB::table('gramota_thanks')->where('id', $id)->where("user_id", $user_id)->first(); //->where('user_id', Auth::user()->id)->first();
        $cert_id = $gt->cert_id;
        $certificate = DB::table("certificate")->where("id", $cert_id)->first();

        $content = $certificate->content;

        $content = str_replace(":certificate_number", $gt->cert_num,  $content);

        $date = date("d.m.Y", strtotime($gt->created_at));

        $content = str_replace(":date", $date,  $content);

        $user = DB::table("users")->where("id", $user_id)->first();

        $content = str_replace(":name", $user->middle_name . ' ' . $user->last_name . ' ' . $user->first_name,  $content);

        $qrCode = new QrCode($gt->cert_num . ', ' . $user->first_name . ' ' . $user->last_name);
        $qrCode->setSize(130);
        $data = $qrCode->writeDataUri();
        $content = str_replace(":qrcode", $data,  $content);

        return view("certificate_template", ['certificate' => $certificate, 'content' => $content]);
    }


    public function konkurs()
    {
        $konkurs = DB::table("konkurs_member")
            ->join('konkurs', 'konkurs.id', '=', 'konkurs_member.konkurs_id')
            ->where('konkurs_member.user_id', Auth::user()->id)
            ->where('konkurs_member.deleted', 0)
            ->select("konkurs_member.id", "konkurs_member.like", "konkurs_member.dislike", "konkurs_member.views", "konkurs.title", "konkurs_member.pos", "konkurs_member.full_name")->get();
        return response()->json($konkurs);
    }
    private function generateGramotaCertNum()
    {
        $cert_num = DB::table('gramota_thanks')->max('cert_num');

        if ($cert_num) {
            $num = intval(str_replace("Z-", "", $cert_num));
            $num = $num + 1;
            return 'Z-' . (str_pad($num, 7, '0', STR_PAD_LEFT));
        } else {
            return 'Z-0000001';
        }
    }
    private function checkUserGramota($u, $min_point)
    {
        if ($u == "") {
            return "ID нөмірді енгізіңіз";
        }
        if (DB::table("gramota_thanks_users")->where("unumber", $u)->where("is_gramota", ($min_point == 0 ? 0 : 1))->count() > 0)
            return "ID бұрын қолданылған";

        $uq = DB::select("SELECT ouo.* from o_user_olimpiad ouo
                            INNER JOIN users u on u.id = ouo.user_id
                            WHERE u.unumber = '$u'");


        if (count($uq) == 0) {
            return 'Бұл қолданушы олимпиадаға қатыспаған';
        }

        $total = DB::select("SELECT AVG(ouo.point*100/ouo.max_point) as total from o_user_olimpiad ouo
        INNER JOIN users u on u.id = ouo.user_id
        WHERE u.unumber = '$u'");

        if (isset($total[0]) && ($total[0]->total >= $min_point)) {
            return "true";
        }
        return "Оқушының ұпайы 80%-ға жетпейді";
    }
    private function uuid()
    {
        return sprintf(
            '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            mt_rand(0, 0xffff),
            mt_rand(0, 0xffff),
            mt_rand(0, 0xffff),
            mt_rand(0, 0x0fff) | 0x4000,
            mt_rand(0, 0x3fff) | 0x8000,
            mt_rand(0, 0xffff),
            mt_rand(0, 0xffff),
            mt_rand(0, 0xffff)
        );
    }

    public function pult24(Request $request)
    {
        $merchant_id = "11109155091382619";
        $username = "11109155091382619";
        $password = "5Rt16n1Gf3dctPF9apJq";
        $url = "https://ecommerce.pult24.kz/payment/create";
        $amount = $request->get("amount");
        $id = DB::table('transaction')->insertGetId([
            'user_id' => Auth::user()->id,
            'amount' => $amount,
            'status' => 'pending',
            'description' => "",
            'source_id' => null,
            'source' => 'pult24'
        ]);
        $data = array(
            "orderId" => "" . $id,
            "merchantId" => $merchant_id,
            "account" => Auth::user()->unumber,
            "amount" => intval($amount) * 100,
            "returnUrl" => "https://edulife.kz/user/payment/complete",
            "callbackUrl" => "https://admin.edulife.kz/api/callback",
            "description" => "charge account " . Auth::user()->email
        );
        $data_string = json_encode($data);
        try {
            //setting the curl parameters.
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_ENCODING, "");
            curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
            curl_setopt($ch, CURLOPT_TIMEOUT, 30);
            curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                "Cache-Control: no-cache",
                'Content-Length: ' . strlen($data_string),
                'Authorization: Basic ' . base64_encode($username . ":" . $password),
            ));
            //return response('order-'.date("Y-m-d H:i:s").date_default_timezone_get());
            $data = curl_exec($ch);
            curl_close($ch);
            $data_array = json_decode($data);
            DB::table('transaction')->where('id', $id)->where('source', 'pult24')->update([
                'description' => $data,
                'source_id' => $data_array->id
            ]);
            return response()->json(['status' => 'success', 'result' => $data_array]);
        } catch (\Exception $ex) {
            return response()->json($ex->getMessage());
        }
    }
    public function callback_pult24(Request $request)
    {
        $ips = array(
            "35.157.105.64",
        );



        if (!in_array($request->ip(), $ips)) {
            echo "noop";
            die();
        }

        $out = false;
        $str_data = file_get_contents('php://input');

        Log::debug($str_data);
        $data = json_decode($str_data);
        Log::debug($data->orderId);
        if ($data->orderId) {
            $tr = DB::table('transaction')->where('id', $data->orderId)->where('source', 'pult24')->where('status', 'pending')->first();
            if ($tr) {
                if ($data->status == 1) {

                    DB::table('transaction')->where('id', $data->orderId)->where('source', 'pult24')->update([
                        'description' => $tr->description . "--" . $str_data,
                        'status' => 'income',

                    ]);
                    $this->calcUserAmount($tr->user_id);
                    $out = true;
                } else {
                    DB::table('transaction')->where('id', $data->orderId)->where('source', 'pult24')->update([
                        'description' => $tr->description . "--" . $str_data,
                    ]);
                }
                return response()->json(["accepted" => $out]);
            }
            return response()->json(["accepted" => $out]);
        }
        return response()->json(["accepted" => $out]);
    }


    public function get_konkurs_member(Request $request)
    {
        $item = DB::table("konkurs_member")->where('id', $request->get("id"))->where("user_id", Auth::user()->id)->first();
        return response()->json($item);
    }

    public function update_konkurs_member(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'history' => 'required',
            'full_name' => 'required',
            'title' => 'required',
            'content' => 'required',
            'konkurs_id' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()]);
        }
        $data = $request->all();
        $km = DB::table('konkurs_member')->where("id", $data['id'])->first();
        if (!$km) {
            return response()->json(["status" => "error", 'message' => 'konkurs member not found']);
        }
        $fileName = $km->profile_image;
        if ($request->hasFile('file')) {
            $fileName = time() . "-" . $request->file->getClientOriginalName();
            $request->file->storeAs('konkurs', $fileName);
            unset($data['file']);
        }
        $doc_name = $km->document_path;
        if ($request->hasFile('document')) {
            $doc_name = time() . "-" . $request->document->getClientOriginalName();
            $request->document->storeAs('konkurs/doc', $doc_name);
            unset($data['document']);
        }

        // $k = DB::table("konkurs")->where('id', $data['konkurs_id'])->first();
        // if($k && Auth::user()->amount < $k->price){
        //     return response()->json(['status'=>"error", "message"=>"Сіздің балансыңыз жеткіліксіз"]);
        // }
        DB::beginTransaction();
        try {
            DB::table('konkurs_member')->where('id', $data['id'])->where('user_id', Auth::user()->id)->update([
                'history' => $data['history'],
                'konkurs_id' => $data['konkurs_id'],
                'title' => $data['title'],
                'content' => $data['content'],
                'full_name' => $data['full_name'],
                'profile_image' => $fileName,
                'document_path' => $doc_name,
            ]);
            // //outcome
            // DB::table('transaction')->insert([
            //     'user_id'=>Auth::user()->id,
            //     'amount'=>$k->price*-1,
            //     'status'=>'outcome',
            //     'description'=>'apply konkurs:'.$k->id.' , konkurs name:'.$k->title,
            //     'source_id'=>$id,
            //     'source'=>'konkurs'
            // ]);
            // $this->calcUserAmount(Auth::user()->id);
            DB::commit();
            return response()->json(["status" => "success", "id" => $data['id']]);
        } catch (\Exception $e) {
            return response()->json(['status' => "error", "message" => $e->getMessage()]);
            DB::rollBack();
        }

        return response()->json(['status' => 'success']);
    }
    public function remove_media_konkurs_member(Request $request)
    {
        $data = $request->all();
        if (isset($data['type']) && isset($data['member_id'])) {
            $member = DB::table('konkurs_member')->where('id', $data['member_id'])->where('user_id', Auth::user()->id)->first();

            if ($data['type'] == "document_path") {
                if (file_exists(public_path('uploads') . "/konkurs/doc/" . $member->document_path)) {
                    unlink(public_path('uploads') . "/konkurs/doc/" . $member->document_path);
                    DB::table('konkurs_member')->where('id', $data['member_id'])->where('user_id', Auth::user()->id)->update([
                        'document_path' => null
                    ]);
                    return response()->json(['status' => 'success']);
                }
            }
            if ($data['type'] == "profile_image") {
                if (file_exists(public_path('uploads') . "/konkurs/" . $member->profile_image)) {
                    unlink(public_path('uploads') . "/konkurs/" . $member->profile_image);
                    DB::table('konkurs_member')->where('id', $data['member_id'])->where('user_id', Auth::user()->id)->update([
                        'profile_image' => null
                    ]);
                    return response()->json(['status' => 'success']);
                }
            }
        }
    }

    public function remove_konkurs_member(Request $request)
    {
        $data = $request->all();
        if (isset($data['id'])) {
            $member = DB::table('konkurs_member')->where('id', $data['id'])->where('user_id', Auth::user()->id)->first();
            if ($member) {
                DB::table('konkurs_member')->where('id', $data['id'])->where('user_id', Auth::user()->id)->update([
                    'deleted' => 1
                ]);
                return response()->json(['status' => 'success']);
            }
        }
        return response()->json(['status' => 'error']);
    }

    public function change_password(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'password' => 'required|between:5,16',
            'new_password' => 'required|between:5,16|confirmed'
        ]);
        if ($validator->fails()) {
            return response()->json(['success' => false, 'errors' => $validator->errors()]);
        }


        $user = User::find(Auth::user()->id);
        if (Hash::check($request->get('password'), $user->password)) {
            $user->password = Hash::make($request->get('new_password'));
            $user->save();
            return response()->json(['success' => true, 'message' => 'Құпия сөз қате']);
        }


        return response()->json(['success' => false, 'errors' => 'Құпия сөз сәтті өзгертілді!']);
    }
}
