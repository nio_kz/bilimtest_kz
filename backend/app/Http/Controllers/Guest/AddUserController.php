<?php


namespace App\Http\Controllers\Guest;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Anam\PhantomMagick\Converter;
use Anam\PhantomLinux\Path;
use Endroid\QrCode\QrCode;

class AddUserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function my_student(){
        $items = DB::table("users")->where("teacher_unumber", Auth::user()->unumber)->orderBy("id", "desc")->paginate(20);
        return response()->json($items);
    }
    public function add_student(Request $request){
        $validator = Validator::make($request->all(), [
            'teacher_id' => 'required',
            'first_name'=> 'required',
            'last_name'=> 'required',
            'school_id'=> 'required'

        ]);

        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()], 200);
        }

        $data = $request->all();

        unset($data['city_id']);
        unset($data['region_id']);
        
        $token = $this->getToken(6);
        $data['email'] = $token."@edulife.kz";
        $data['password']=Hash::make($token);
        $data['unumber'] = $this->generateBarcode();
        $data['user_role'] = "user";
        $data['user_type'] = "high_school_student";
        $data['teacher_unumber'] = $data['teacher_id'];
        unset($data['teacher_id']);
        DB::table("users")->insert($data);
        return response()->json(['status'=>'success']);

    }

    public function get_olimpiad(Request $request){
        $data = $request->all();
        $item = DB::table("o_olimpiad")->where("lesson_id", $data['lesson_id'])->where("class_id", $data['class_id'])->where("deleted", 0)->where("is_active", 1)->first();
        return response()->json($item);
    }
    private function generateBarcode() {
        $unumber = mt_rand(10000000, 99999999);
        $users = DB::table('users')->where('unumber', $unumber)->get();
        if(count($users)>0){
            $this->generateBarcode();
        }else
        {
            return $unumber;
        }
    }
    private function getToken($length){
        $token = "";
        $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
        $codeAlphabet.= "0123456789";
        $max = strlen($codeAlphabet); // edited

        for ($i=0; $i < $length; $i++) {
            $token .= $codeAlphabet[random_int(0, $max-1)];
        }

        return $token;
    }
}