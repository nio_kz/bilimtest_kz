<?php

namespace App\Http\Controllers\Guest;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
class IndexController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['only' => ['post_konkurs_member', 'get_city_school']]);
    
    }
    public function slider($section, $is_video){
        $slides = DB::table("slider")->where('deleted', 0)->where("is_video_or_image", $is_video)->where('slider_section', $section)->get();
        return response()->json($slides);
    }

    public function banner($section){
        $banner = DB::table("banner")->where('deleted', 0)->where("section", $section)->first();
        return response()->json($banner);
    }
    public function konkurs_tab(){
        $konkurs = DB::table("konkurs")->where('deleted', 0)->limit(4)->get();
        return response()->json($konkurs);
    }
    public function konkurs(){
        $paginate = DB::table("konkurs")->where('deleted', 0)->whereIn('status', [0,2])->paginate(16);
        return response()->json($paginate);
    }
    
    public function konkurs_detail($id){
        $konkurs = DB::table("konkurs")->where('deleted', 0)->where('id', $id)->whereIn('status', [0,2])->first();
        if($konkurs){
            return response()->json(['konkurs'=>$konkurs]);
        }
        return response()->json(['status'=>'expired']);
        
    }

    public function konkurs_members(Request $request, $id){
        $members = DB::table("konkurs_member")
        ->where('konkurs_id', $id)
        ->where('deleted', 0);
        if($request->get("q", null)){
            $members=$members->where('full_name', 'like', '%'.$request->get("q", null).'%')
            ->where('title', 'like', '%'.$request->get("q", null).'%');
        }
        $members=$members->orderByRaw('konkurs_member.like desc, konkurs_member.views desc')->paginate(20);
        return response()->json($members);
    }

    public function news(){
        $paginate = DB::table("news")->where('deleted', 0)->orderBy("created_at", "desc")->paginate(16);
        return response()->json($paginate);
    }
    public function news_detail($id){
        $news = DB::table("news")->where('deleted', 0)->where('id', $id)->first();
        return response()->json($news);
    }
    public function top_news(){
        $paginate = DB::table("news")->where('deleted', 0)->orderBy("read", "desc")->limit(5)->get();
        return response()->json($paginate);
    }
    public function home_news(){
        $paginate = DB::table("news")->where('deleted', 0)->orderBy("created_at", "desc")->limit(3)->get();
        return response()->json($paginate);
    }
    public function konkurs_members_home(){
        $paginate = DB::table("konkurs_member")->where('deleted', 0)->orderBy("created_at", "desc")->limit(4)->get();
        return response()->json($paginate);
    }

    public function location($id=null){
        $location = DB::table("location")->where('deleted', 0)->where('parent_id', $id)->get();
        
        return response()->json($location);
    }
    public function school($id){
        $school = DB::table("school")->where('deleted', 0)->where('location_id', $id)->get();
        
        return response()->json($school);
    }

    public function get_city_school(){
        $school_id = Auth::user()->school_id;
        $school = DB::table("school")->where("id", $school_id)->first();
        $schools = DB::table("school")->where("location_id", $school->location_id)->get();
        $city_id = $school->location_id;
        $city = DB::table("location")->where("id", $city_id)->first();
        $cities = DB::table("location")->where("parent_id", $city->parent_id)->get();
        $region_id =  $city->parent_id;

        return response()->json([
            'school_id'=>$school_id,
            'schools'=>$schools,
            'city_id'=>$city_id,
            'cities'=>$cities,
            'region_id'=>$region_id
        ]);
    }

    public function getUserIpAddr(){
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if(isset($_SERVER['REMOTE_ADDR']))
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';    
        return $ipaddress;
     }              
    public function konkurs_like($id, $obj, Request $request){
        $ip = $this->getUserIpAddr();
        $km = DB::table("konkurs_member")->where('id', $id)->first();
        $k = DB::table("konkurs")->where('id', $km->konkurs_id)->first();
        if($k && $k->status==2){
            return response()->json(['status'=>'error', 'message'=>'Құрметті қолдаушылар!  "Абай оқулары"  байқауына видео қабылдау 15:00 аяқталады.  Дауыс беру 23:59 аяқталандығын хабарлаймыз!']);
        }
        $rows = DB::table("konkurs_rate")
        ->where("konkurs_member_id", $id)
        ->where("obj", $obj)
        ->where("ip", $ip)->get();
        if(count($rows)>0){
            DB::table("konkurs_rate")
            ->where("konkurs_member_id", $id)
            ->where("obj", $obj)
            ->where("ip", $ip)->delete();
        }else{
            DB::table("konkurs_rate")->insert([
                'ip'=>$ip,
                'konkurs_member_id'=>$id,
                'obj'=>$obj
            ]);
        }

        
        $like = DB::table("konkurs_rate")
        ->where("konkurs_member_id", $id)
        ->groupBy("konkurs_member_id")
        ->select(DB::raw("SUM(IF(obj='like', 1, 0)) as llike, SUM(IF(obj='dislike', 1, 0)) as dlike"))->first();
        if($like){
            DB::table("konkurs_member")->where("id", $id)->update([
                "like"=>$like->llike,
                "dislike"=>$like->dlike,
                ]);
        }
        
        return response()->json($like);
    }
    public function konkurs_member(Request $request){
        $k = DB::table("konkurs")->where('id', $request->get("konkurs"))->first();
        if($k && $k->status==2){
            return response()->json(['status'=>'error', 'message'=>'Құрметті қолдаушылар!  "Абай оқулары"  байқауына видео қабылдау 15:00 аяқталады.  Дауыс беру 23:59 аяқталандығын хабарлаймыз!']);
        }
        $item = DB::table("konkurs_member")->where('id', $request->get("member"))
        ->where('konkurs_id', $request->get("konkurs"))->first();
        if($this->confirm_view($item->id)){
            DB::table("konkurs_member")->where('id', $request->get("member"))
            ->where('konkurs_id', $request->get("konkurs"))->update(['views'=>$item->views+1]);
        }
       
        return response()->json($item);
    }

    private function confirm_view($id){

        $ip = $this->getUserIpAddr();
        $rows = DB::table("konkurs_rate")
        ->where("konkurs_member_id", $id)
        ->where("obj", "views")
        ->where("ip", $ip)->get();
        if(count($rows)>0){
            return false;
        }else{
            DB::table("konkurs_rate")->insert([
                'ip'=>$ip,
                'konkurs_member_id'=>$id,
                'obj'=>"views"
            ]);
        } 
        return true;
    }
    public function post_konkurs_member(Request $request){
        $validator = Validator::make($request->all(), [
            'file' => 'required',
            'history'=>'required',
            'full_name'=>'required',
            'title'=>'required',
            'content'=>'required',
            'konkurs_id'=>'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()]);
        }
        $data = $request->all();
        $fileName = null;
        if ($request->hasFile('file')) {
            $fileName = time()."-".$request->file->getClientOriginalName();
            $request->file->storeAs('konkurs', $fileName);
            unset($data['file']);
        }
        $doc_name = null;
        if ($request->hasFile('document')) {
            $doc_name = time()."-".$request->document->getClientOriginalName();
            $request->document->storeAs('konkurs/doc', $doc_name);
            unset($data['document']);
        }
       
        $k = DB::table("konkurs")->where('id', $data['konkurs_id'])->first();
        if($k && Auth::user()->amount < $k->price){
            return response()->json(['status'=>"error", "message"=>"Сіздің балансыңыз жеткіліксіз"]);
        }
        DB::beginTransaction();
        try{
           $id = DB::table('konkurs_member')->insertGetId([
                'user_id'=>Auth::user()->id,
                'history'=>$data['history'],
                'konkurs_id'=>$data['konkurs_id'],
                'title'=>$data['title'],
                'content'=>$data['content'],
                'full_name'=>$data['full_name'],
                'profile_image'=>$fileName,
                'document_path'=>$doc_name,
            ]);
            //outcome
            DB::table('transaction')->insert([
                'user_id'=>Auth::user()->id,
                'amount'=>$k->price*-1,
                'status'=>'outcome',
                'description'=>'apply konkurs:'.$k->id.' , konkurs name:'.$k->title,
                'source_id'=>$id,
                'source'=>'konkurs'
            ]);
            $this->calcUserAmount(Auth::user()->id);
            DB::commit();
            return response()->json(["status"=>"success", "id"=>$id]);
        }catch(\Exception $e){
            return response()->json(['status'=>"error", "message"=>$e->getMessage()]);
            DB::rollBack();
        }

        return response()->json(['status'=>'success']);
    }

    private function calcUserAmount($user_id){
        $total = DB::table("transaction")->where('user_id', $user_id)->whereIn('status', ['income', 'outcome'])->sum("amount");
        DB::table("users")->where('id', $user_id)->update(['amount'=>$total]);
    }
    
}
