<?php


namespace App\Http\Controllers\Guest;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;


use App\Http\Controllers\Controller;

class LoginController extends Controller
{
    public function logout(Request $request){
        $this->guard()->logout();
        return response()->json([
            	'status'=>"success"
            ]);
    }

    public function login(Request $request){
        //validate incoming request 
        
        $validator = Validator::make($request->all(), [
            'email' => 'required|string',
            'password' => 'required|string',
        ]);
        
        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()], 200);
         }
     

        $credentials = $request->only(['email', 'password']);

        if (! $token = Auth::attempt($credentials)) {
            return response()->json(['message' => 'Пошта немесе құпия сөз қате'], 200);
        }
        $this->calcUserAmount(Auth::user()->id);
        if(Auth::user()->user_role=="user"){
            return $this->respondWithToken($token);
        }
        if(Auth::user()->user_role=="admin"){
            return $this->respondWithToken($token);
        }
        if(Auth::user()->user_role=="super_admin"){
            return $this->respondWithToken($token);
        }
        return response()->json(['message' => 'role_not_defined'], 200);

        
    }

    public function register(Request $request){
        $validator = Validator::make($request->all(), [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'email' => 'required|email|unique:users',
            'user_type' => 'required',
            'gender' => 'required',
            'school_id' => 'required',
            'password' => 'required|confirmed',
            
        ]);
        
        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()], 200);
         }
        // register
         $data = $request->all();
         unset($data['password_confirmation']);
         $data['password']=Hash::make($data['password']);
         $data['unumber'] = $this->generateBarcode();
         $data['user_role'] = "user";
         DB::table("users")->insert($data);
         return response()->json(['status'=>'success']);
    }
    private function generateBarcode() {
        $unumber = mt_rand(10000000, 99999999);
        $users = DB::table('users')->where('unumber', $unumber)->get();
        if(count($users)>0){
            $this->generateBarcode();
        }else
        {
            return $unumber;
        }
    }

    public function forget_password(Request $request){
        
        if($request->get("email",null)==null){
            return response()->json(["status"=>'error', 'message'=>'Почтаңызды қате енгіздіңіз']);
        }

        if (!filter_var($request->get("email",null), FILTER_VALIDATE_EMAIL)) {
            return response()->json(["status"=>'success', 'message'=>'Почтаңызды қате енгіздіңіз']);
        }

        $user = DB::table("users")->where("email", $request->get("email"))->first();
        if(!$user){
            return response()->json(["status"=>'success', 'message'=>'Енгізілген почта табылмады']);
        }
        $token = $this->random_string(150);
        DB::table("users")->where("email", $request->get("email"))->update([
            'token'=> $token
        ]);
        // Create the Transport
        $transport = (new \Swift_SmtpTransport('mail.nio.kz', 25, 'tls'))
        ->setUsername('webmaster@nio.kz')
        ->setPassword('nio@root123')
        ->setStreamOptions(array('ssl' => array('allow_self_signed' => true, 'verify_peer' => false, 'verify_peer_name'=>false)));
        ;

        // Create the Mailer using your created Transport
        $mailer = new \Swift_Mailer($transport);

        // Create a message
        $message = (new \Swift_Message('құпия сөзімді ұмыттым'))
        ->setFrom(['webmaster@nio.kz' => 'edulife.kz'])
        ->setTo([$user->email])
        ->setContentType("text/html")
        ->setBody(view("forget_password", ['token'=>$token]))
        ;

        // Send the message
        try{
            $result = $mailer->send($message);
        }catch(\Exception $e){
            return response()->json(["status"=>'error', 'message'=>$e->getMessage()]);
        }
        
        if($result==1){
            return response()->json(["status"=>'success', 'message'=>'Почтаңызды тексеріңіз']);
        }
    }

    public function reset_password(Request $request){
        $validator = Validator::make($request->all(), [
            'email' => 'required|string',
            'token' => 'required',
            'password' => 'required|confirmed',
            
        ]);
        $data = $request->all();
        $user = DB::table("users")->where('email', $data['email'])->where('token', $data['token'])->first();
        
        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()], 200);
         }
        
         $user = DB::table("users")->where('email', $data['email'])->where('token', $data['token'])->first();
         if($user){
            DB::table("users")->where('email', $data['email'])->where('token', $data['token'])->update([
                'token'=>null,
                'password'=>Hash::make($data['password'])
            ]);
            return response()->json(['status'=>'success']);
         }else{
            return response()->json(['status'=>'error', 'message'=>'Енгізілген почта табылмады']);
         }

         
    }
    private function random_string($length) {
        $key = '';
        $keys = array_merge(range(0, 9), range('a', 'z'));
    
        for ($i = 0; $i < $length; $i++) {
            $key .= $keys[array_rand($keys)];
        }
    
        return $key;
    }

    private function calcUserAmount($user_id){
        $total = DB::table("transaction")->where('user_id', $user_id)->whereIn('status', ['income', 'outcome'])->sum("amount");
        DB::table("users")->where('id', $user_id)->update(['amount'=>$total]);
    }
}