<?php

namespace App\Http\Controllers\Guest;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Response;
use Endroid\QrCode\QrCode;
use Anam\PhantomMagick\Converter;

class LessonPlanController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth',  ['except' => [
            'lesson',
            'classes',
            'plan_type',
            'lesson_plan_download',
            'lesson_plan_preview',
            'lesson_plans_public',
            'download_certificate_html',
            'lesson_plan_redirect',
            'lesson_plan_detail'
        ]]);
    }

    public function lesson_plans_public(Request $request){
        $lesson = $request->get("lesson", null);
        $class = $request->get("class", null);
        $plan = $request->get("plan", null);
        $q = $request->get("q", null);
        $plans = DB::table("lp_lesson_plan")
        ->join('lp_lesson', 'lp_lesson.id', '=', 'lp_lesson_plan.lesson_id')
        ->join('lp_class', 'lp_class.id', '=', 'lp_lesson_plan.class_id')
        ->join('lp_plan_type', 'lp_plan_type.id', '=', 'lp_lesson_plan.plan_type_id')

        ;
        if($lesson){
            $plans = $plans->where('lp_lesson_plan.lesson_id', $lesson);
        }
        if($class){
            $plans = $plans->where('lp_lesson_plan.class_id', $class);
        }
        if($plan){
            $plans = $plans->where('lp_lesson_plan.plan_type_id', $plan);
        }
        if($q){
            $plans = $plans->whereRaw('lp_lesson_plan.title like "%'.$q.'%" and lp_lesson_plan.descr like "%'.$q.'%"');
        }
        $plans = $plans->where('lp_lesson_plan.deleted', 0)->orderBy('lp_lesson_plan.created_at', 'desc')
        ->select("lp_lesson_plan.*", "lp_class.name as class_name", "lp_lesson.name as lesson_name", "lp_plan_type.name as plan_name")
        ->paginate(12);
        return response()->json($plans);
    }

    
    public function lesson_plan_detail($id){
        $item = DB::table("lp_lesson_plan")->where('id', $id)->first();
        return response()->json($item);
    }
    public function lesson_plan_redirect($slug){
        $item = DB::table("lp_lesson_plan")->where('slug', $slug)->first();
        if($item){
            return response()->json(['id'=>$item->id]);
        }
        return response()->json(['status'=>'not_found']);
    }

    public function lesson_plans(){
        $plans = DB::table("lp_lesson_plan")->where('deleted', 0)->where('user_id', Auth::user()->id)->paginate(20);
        return response()->json($plans);
    }
    public function lesson_plan_destory(Request $request){
        $id = $request->get("id");
        DB::table("lp_lesson_plan")->where('id', $id)->where('user_id', Auth::user()->id)->update(['deleted'=>1]);

        return response()->json(['status'=>"success"]);

    }

    public function lesson_plan_download(Request $request){
        $id = $request->get("id");
        $item = DB::table("lp_lesson_plan")->where('id', $id)->first();
        if(file_exists(public_path()."/uploads/docs/".$item->doc_path)){
            return response(file_get_contents(public_path()."/uploads/docs/".$item->doc_path));//->download(public_path()."/uploads/docs/".$item->doc_path);
        }elseif(file_exists(public_path()."/uploads/".$item->doc_path)){
            return response(file_get_contents(public_path()."/uploads/".$item->doc_path));
        }
        else{
            return response()->json(['message'=>"file not found"]);
        }
    }

    public function lesson_plan_preview(Request $request){
        $types = [
            "doc"=>"application/msword",
            "dot"=>"application/msword",

            "docx"=>"application/vnd.openxmlformats-officedocument.wordprocessingml.document",
            "dotx"=>"application/vnd.openxmlformats-officedocument.wordprocessingml.template",
            "docm"=>"application/vnd.ms-word.document.macroEnabled.12",
            "dotm"=>"application/vnd.ms-word.template.macroEnabled.12",

            "xls"=>"application/vnd.ms-excel",
            "xlt"=>"application/vnd.ms-excel",
            "xla"=>"application/vnd.ms-excel",

            "xlsx"=>"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
            "xltx"=>"application/vnd.openxmlformats-officedocument.spreadsheetml.template",
            "xlsm"=>"application/vnd.ms-excel.sheet.macroEnabled.12",
            "xltm"=>"application/vnd.ms-excel.template.macroEnabled.12",
            "xlam"=>"application/vnd.ms-excel.addin.macroEnabled.12",
            "xlsb"=>"application/vnd.ms-excel.sheet.binary.macroEnabled.12",

            "ppt"=>"application/vnd.ms-powerpoint",
            "pot"=>"application/vnd.ms-powerpoint",
            "pps"=>"application/vnd.ms-powerpoint",
            "ppa"=>"application/vnd.ms-powerpoint",

            "pptx"=>"application/vnd.openxmlformats-officedocument.presentationml.presentation",
            "potx"=>"application/vnd.openxmlformats-officedocument.presentationml.template",
            "ppsx"=>"application/vnd.openxmlformats-officedocument.presentationml.slideshow",
            "ppam"=>"application/vnd.ms-powerpoint.addin.macroEnabled.12",
            "pptm"=>"application/vnd.ms-powerpoint.presentation.macroEnabled.12",
            "potm"=>"application/vnd.ms-powerpoint.template.macroEnabled.12",
            "ppsm"=>"application/vnd.ms-powerpoint.slideshow.macroEnabled.12",

            "mdb"=>"application/vnd.ms-access"
        ];
        $id = $request->get("id");
        $item = DB::table("lp_lesson_plan")->where('id', $id)->first();
        try{
            
            if(file_exists(public_path()."/uploads/docs/".$item->doc_path)){
                if(isset($types[$item->doc_type])){
                    return response(file_get_contents(public_path()."/uploads/docs/".$item->doc_path))
                    ->header('Cache-Control', 'no-store, no-cache, must-revalidate, post-check=0, pre-check=0')
                    ->header('X-Frame-Options', 'https://docs.google.com/')
                    ->header('Content-Type', $types[$item->doc_type]);//->download(public_path()."/uploads/docs/".$item->doc_path);

                }else{
                    return response(file_get_contents(public_path()."/uploads/docs/".$item->doc_path));
                }
                // return Response::download(public_path()."/uploads/docs/".$item->doc_path, "doc.docx");
                
            }elseif(file_exists(public_path()."/uploads/".$item->doc_path)){
                if(isset($types[$item->doc_type])){
                    return response(file_get_contents(public_path()."/uploads/".$item->doc_path))
                    ->header('Cache-Control', 'no-store, no-cache, must-revalidate, post-check=0, pre-check=0')
                    ->header('X-Frame-Options', 'https://docs.google.com/')
                    ->header('Content-Type', $types[$item->doc_type]);//->download(public_path()."/uploads/docs/".$item->doc_path);

                }else{
                    return response(file_get_contents(public_path()."/uploads/".$item->doc_path));
                }
            }
            else{
                return response()->json(['message'=>"file not found"]);
            }
        }catch(\Exception $e){
            var_dump($e->getMessage());
        }
        
    }

    public function add_lesson_plan(Request $request){
        $validator = Validator::make($request->all(), [
            'lesson_id' => 'required',
            'class_id' => 'required',
            'plan_type_id' => 'required',
            'title' => 'required',
            'descr' => 'required',
            'full_name' => 'required',
            'email' => 'required',
            'work_address' => 'required',
            'work_level' => 'required',
            'doc_type' => 'required',
            'doc_path' => 'required',
            
        ]);
        
        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()], 200);
        }

        
        
        $data = $request->all();
       
        DB::beginTransaction();
        try{
            $data['user_id'] = Auth::user()->id;
            DB::table("lp_lesson_plan")->insert($data);
            DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            return response()->json(['status'=>"error", "message"=>$e->getMessage()]);
        }
        

        return response()->json(['status'=>"success"]);

    }

    public function download_certificate($id, $type){
        
        if($id){
            $lp_plan = DB::table("lp_lesson_plan")->where("id", $id)->first();
            $lp = DB::table("lp_lesson")->where("id", $lp_plan->lesson_id)->first();
            $certificate = DB::table("certificate")->where("id", $lp->certificate_id)->first();

            $c = new Converter();
            $c->source("https://admin.edulife.kz/api/lesson-plan-download-certificate-html/".$id);
            if($type=="image"){
                $c->toJpg();
                if($certificate->orientation=='landscape'){
                    $c->width(1122);
                    $c->height(793);
                }else{
                    $c->width(793);
                    $c->height(1122);
                }
            }
            if($type=="pdf"){
                $w=$h=0;
                if($certificate->orientation=='landscape'){
                    $w = 1122;
                    $h = 793;
                }else{
                    $w = 793;
                    $h = 1122;
                }
                $options = [
                    'width' => $w . 'px',
                    'height' => ($h + 5) . 'px',
                    'zoomfactor' => 1,
                    'orientation' => $certificate->orientation,
                    'margin' => '0'
                ];
                $c->toPDF($options);
            }
            
            
            $c->quality(100);
            if($type=="image"){
                $c->save(public_path().'/uploads/tmp/'.$id.'.jpg');
                $content = file_get_contents(public_path().'/uploads/tmp/'.$id.'.jpg');
                if(file_exists(public_path().'/uploads/tmp/'.$id.'.jpg')){
                    unlink(public_path().'/uploads/tmp/'.$id.'.jpg');
                }
            }
            if($type=="pdf"){
                $c->save(public_path().'/uploads/tmp/'.$id.'.pdf');
                $content = file_get_contents(public_path().'/uploads/tmp/'.$id.'.pdf');
                if(file_exists(public_path().'/uploads/tmp/'.$id.'.pdf')){
                    unlink(public_path().'/uploads/tmp/'.$id.'.pdf');
                }
            }

            return $content;
        }
    }
    public function download_certificate_html($id){
        if($id){
            $lp_plan = DB::table("lp_lesson_plan")->where("id", $id)->first();
            if(!$lp_plan){
                return "not found";
            }
            $lp = DB::table("lp_lesson")->where("id", $lp_plan->lesson_id)->first();
            $certificate = DB::table("certificate")->where("id", $lp->certificate_id)->first();

            $content = $certificate->content;
            // $content = str_replace(":certificate_number", $ouo->cert_num,  $content);
            $date = date("d.m.Y", strtotime($lp_plan->created_at));
            $content = str_replace(":date", $date,  $content);
            // $content = str_replace(":name", $user->first_name.' '.$user->last_name,  $content);
            // $content = str_replace(":teacher_name", $user->user_type == "student" ? "teacher_name":'',  $content);
            $content = str_replace(":certificate_number", date("Ymd", strtotime($lp_plan->created_at)).$lp_plan->id,  $content);
            $content = str_replace(":name", $lp_plan->full_name,  $content);
            $content = str_replace(":lesson_name", '"'.$lp_plan->title.'"',  $content);
            $qrCode = new QrCode($lp_plan->id. ', '.$lp_plan->full_name.', '.$lp_plan->title);
            $qrCode->setSize(130);
            $data = $qrCode->writeDataUri();
            $content = str_replace(":qrcode", $data,  $content);
            return view("lp_certificate_template", ['certificate'=>$certificate, 'content'=>$content]);

        }
    }

    public function lesson(){
        // $lessons = [];
        $lesson = DB::table("lp_lesson")
        ->join("lp_lesson_plan", "lp_lesson_plan.lesson_id", "=", "lp_lesson.id")
        ->where('lp_lesson.deleted', 0)
        
        ->select(DB::raw("lp_lesson.id, lp_lesson.name , count(lp_lesson_plan.id) as total"))
        ->groupBy("lp_lesson.id", "lp_lesson.name")
        ->get();
        // foreach($lesson as $l){
        //     $lessons[]=[
        //         'id'=>$l->id,
        //         'name'=>$l->name
        //     ];
        // }
        return response()->json($lesson);
    }

    public function plan_type(Request $request){
        $items = DB::table("lp_plan_type")
        ->join("lp_lesson_plan", "lp_lesson_plan.plan_type_id", "=", "lp_plan_type.id");
        if($request->get("lesson", null)){
            $items = $items->where("lp_lesson_plan.lesson_id", $request->get("lesson"));
        }
        $items = $items->where('lp_plan_type.deleted', 0)
        
        ->select(DB::raw("lp_plan_type.id, lp_plan_type.name , count(lp_lesson_plan.id) as total"))
        ->groupBy("lp_plan_type.id", "lp_plan_type.name")
        ->get();
        return response()->json($items);
    }

    public function classes(Request $request){

        $items = DB::table("lp_class")
        ->join("lp_lesson_plan", "lp_lesson_plan.class_id", "=", "lp_class.id");
        if($request->get("lesson", null)){
            $items = $items->where("lp_lesson_plan.lesson_id", $request->get("lesson"));
        }
        $items = $items->where('lp_class.deleted', 0)
        
        ->select(DB::raw("lp_class.id, lp_class.name , count(lp_lesson_plan.id) as total"))
        ->groupBy("lp_class.id","lp_class.name")
        ->get();
        return response()->json($items);
    }

    

    public function upload(Request $request){
        
        $validator = Validator::make($request->all(), [
            'file' => 'required|file|max:30720',
            
        ]);
        
        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()], 200);
        }
         
        $fileName = time()."-".$request->file->getClientOriginalName();

        $request->file->storeAs('docs', $fileName);

        return response()->json(['filename'=>$fileName, 'doc_ext'=>strtolower($request->file->getClientOriginalExtension())]);
    }
    public function upload_remove(Request $request){
        
        if($request->get('filename') && file_exists(public_path()."/uploads/docs/".$request->get("filename"))){
            unlink(public_path()."/uploads/docs/".$request->get("filename"));
            return response()->json(['status'=>'success']);
        }
        return response()->json(['status'=>'error']);
    }

    private function get_lesson_types($id){
        return DB::table("lp_lesson")->where('parent_id', $id)->where('deleted', 0)->get();
    }
    private function calcUserAmount($user_id){
        $total = DB::table("transaction")->where('user_id', $user_id)->sum("amount");
        DB::table("users")->where('id', $user_id)->update(['amount'=>$total]);
    }
}