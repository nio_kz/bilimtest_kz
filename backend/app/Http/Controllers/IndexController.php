<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Endroid\QrCode\QrCode;

class IndexController extends Controller
{
    public function fix_amount()
    {
        $users = DB::table('users')->where('amount', '>', 0)->get();
        foreach ($users as $user) {
            $this->calcUserAmount($user->id);
        }
        return "done";
    }

    public function certificates(Request $request)
    {
        $name = $request->get("name", "");
        if ($name != "") {
            $items = DB::table("konkurs_member")->where('konkurs_id', 1)->whereIn('user_id', [589203, 600477])->where('full_name', 'like', '%' . $name . '%')->orderBy('full_name', 'asc')->get();
        } else {
            $items = DB::table("konkurs_member")->where('konkurs_id', 1)->whereIn('user_id', [589203, 600477])->orderBy('full_name', 'asc')->get();
        }


        return view("certificate_links", ['items' => $items, 'name' => $name]);
    }

    public function lesson_plan_fix()
    {
        // $items = DB::table("nio_db.sabaq_attachments")->get();
        // DB::beginTransaction();
        // try{
        //     foreach($items as $item){

        //         $user = DB::table("users")->where('id', $item->user_id)->first();
        //         if(!$user){
        //             $n_user = DB::table("nio_db.users")->where("id", $item->user_id)->first();
        //             if($n_user){

        //                 DB::table("users")->insert([
        //                     'id'=>$n_user->id,
        //                     'unumber'=>$n_user->unumber,
        //                     'email'=>$n_user->email,
        //                     'password'=>$n_user->password,
        //                     'first_name'=>$n_user->first_name,
        //                     'last_name'=>$n_user->last_name,
        //                     'phone'=>$n_user->phone,
        //                     'thumbnail'=>$n_user->thumbnail,
        //                     'location_id'=>$n_user->location_id,
        //                     'school_id'=>$n_user->school_id,
        //                     'user_type'=>$n_user->user_type,
        //                     'user_role'=>'user',
        //                     'gender'=>null,
        //                     'social_id'=>null,
        //                     'social_provider'=>null,
        //                     'deleted'=>0,
        //                     'amount'=>$n_user->amount,
        //                 ]);
        //             }

        //         }
        //         DB::table("lp_lesson_plan")->insert([
        //             'id'=> $item->id,
        //             'class_id'=> $item->group_id,
        //             'lesson_id'=> $item->lesson_id,
        //             'plan_type_id'=> $item->post_type_id ? $item->post_type_id:15,
        //             'title'=> $item->title_kz,
        //             'descr'=> $item->content_kz,
        //             'full_name'=> $user ? $user->first_name.' '.$user->last_name : $n_user->first_name.' '.$n_user->last_name,
        //             'email'=> $item->email ? ($user ? $user->email:$n_user->email):'',
        //             'work_address'=> $item->user_work,
        //             'work_level'=> $item->user_work_role,
        //             'doc_path'=> $item->attachment_path,
        //             'doc_type'=> $item->doc_type,
        //             'user_id'=> $item->user_id,
        //             'deleted'=> 0,
        //             'download'=> $item->download,
        //             'id_number'=> $item->id_number
        //         ]);
        //     }
        //     DB::commit();
        // }catch(\Exception $e){
        //     DB::rollback();

        //     // and throw the error again.
        //     throw $e;
        // }

    }
    public function index(Request $request)
    {

        $token = $request->session()->get("token", null);
        return view("start", ['token' => $token]);
    }

    public function settings(Request $request)
    {

        $settings = DB::table("settings")->get();
        return response()->json($settings);
    }


    public function login_redirect(Request $request)
    {
        $request->session()->remove("token");
        $request->session()->save();
        return redirect("/");
    }

    public function login(Request $request)
    {
        $data = $request->all();
        $admins = ["kcoder.2010@gmail.com", "zhanibyek@gmail.com", "lashka_06@mail.ru", "jax_93@bk.ru"];
        if (in_array($data["email"], $admins) && $token = $this->guard()->attempt(['email' => $data['email'], 'password' => $data['password']])) {
            $request->session()->put("token", $token);
            $request->session()->save();
            return redirect("/");
        } else {
            return redirect("/");
        }
    }

    public function logout(Request $request)
    {
        if ($request->isMethod('POST')) {
            Auth::logout();
            return response()->json(['status' => 'success']);
        } else {
            $request->session()->remove("token");
            $request->session()->save();
        }

        return redirect("/");
    }

    public function me()
    {

        return response()->json($this->guard()->user());
    }

    protected function responseWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => $this->guard()->factory()->getTTL() * 60
        ]);
    }
    public function guard()
    {
        return Auth::guard();
    }

    public function media(Request $request)
    {

        if (file_exists(public_path() . "/uploads/" . $request->get("filename", "no-image.png"))) {
            return response(file_get_contents(public_path() . "/uploads/" . $request->get("filename", "no-image.png")));
        }

        return response(file_get_contents(public_path() . "/uploads/no-image.png"));
    }




    private $response_codes_qiwi = array(
        0 => "No errors (success)",
        1 => "Unknown type of request",
        2 => "The subscriber can not be found",
        3 => "Invalid payment amount",
        4 => "Invalid value payment numbers",
        5 => "Incorrect date value",
        6 => "Successful payment with this number can not be found",
        7 => "Payment with that number is canceled",
        8 => "Payment state indefinitely",
        9 => "Payment can not be canceled",
        10 => "Other errors"

    );

    private $response_codes_kassa24 = array(
        0 => "Нет ошибки (успех)",
        1 => "Неизвестный тип запроса",
        2 => "Абонент не найден",
        3 => "Неверная сумма платежа",
        4 => "Неверное значение номера платежа",
        5 => "Неверное значение даты",
        10 => "Прочие ошибки"

    );

    private $response_codes_kaspi = array(
        0 => "абонент/счёт/заказ найден и доступен для пополнения/оплаты",
        1 => "'абонент/счёт не найден' или 'заказ не найден', если запрос check был на проверку состояния заказа",
        2 => "заказ отменен",
        3 => "заказ уже оплачен",
        4 => "платеж в обработке",
        5 => "Другая ошибка провайдера"

    );


    private $actions = array("check", "pay", "status", "cancel");
    private $actions_kassa24 = array("check", "payment", "status", "cancel");

    public function qiwi(Request $request)
    {
        $ips = array(
            "89.218.54.34",
            "89.218.54.36",
            "212.154.215.82",
            "79.142.55.227",
            "202.21.106.125",
            "90.143.35.133",
            '103.10.21.60',
            '66.181.185.172',
            '185.146.1.121',
            '66.181.191.45'
        );



        if (!in_array($request->ip(), $ips)) {
            return abort(403, 'Unauthorized action.');
        }

        $inputs = Input::all();
        if (!isset($inputs["command"])) {
            $xml = '<?xml version="1.0" encoding="utf-8"?>
            <response>
            <osmp_txn_id>' . $inputs['txn_id'] . '</osmp_txn_id>
            <result>1</result>
            <comment>' . $this->response_codes_qiwi[1] . '</comment>
            </response>
                        ';
            return response($xml, 200)->header('Content-Type', 'application/xml');
        }
        if (!in_array($inputs["command"], $this->actions)) {
            $xml = '<?xml version="1.0" encoding="utf-8"?>
                        <response>
                        <osmp_txn_id>' . $inputs['txn_id'] . '</osmp_txn_id>
                        <result>1</result>
                        <comment>' . $this->response_codes_qiwi[1] . '</comment>
                        </response>
                        ';
            return response($xml, 200)->header('Content-Type', 'application/xml');
        }


        if (isset($inputs['command']) && $inputs["command"] == "check") {

            $user = DB::table("users")->where("unumber", $inputs["account"])->where("deleted", 0)->first();
            if ($user) {
                $xml = '<?xml version="1.0" encoding="utf-8"?>
                        <response>
                        <osmp_txn_id>' . $inputs['txn_id'] . '</osmp_txn_id>
                        <result>0</result>
                        <comment>OK</comment>
                        </response>
                        ';
                return response($xml, 200)->header('Content-Type', 'application/xml');
            } else {
                $xml = '<?xml version="1.0" encoding="utf-8"?>
                        <response>
                        <osmp_txn_id>' . $inputs['txn_id'] . '</osmp_txn_id>
                        <result>2</result>
                        <comment>' . $this->response_codes_qiwi[2] . '</comment>
                        </response>
                        ';
                return response($xml, 200)->header('Content-Type', 'application/xml');
            }
        }


        if (isset($inputs['command']) && $inputs["command"] == "pay") {
            if (\DateTime::createFromFormat('YmdHis', $inputs["txn_date"]) === FALSE) {
                $xml = '<?xml version="1.0" encoding="utf-8"?>
                        <response>
                        <osmp_txn_id>' . $inputs['txn_id'] . '</osmp_txn_id>
                        <result>1</result>
                        <comment>' . $this->response_codes_qiwi[5] . '</comment>
                        </response>
                        ';
                return response($xml, 200)->header('Content-Type', 'application/xml');
            }

            $user = DB::table("users")->where("unumber", $inputs["account"])->where("deleted", 0)->first();
            if ($user) {
                DB::table("transaction")->insert([
                    'user_id'       => $user->id,
                    'amount'        => $inputs['sum'],
                    'status'        => 'income',
                    'description'   => 'terminal: qiwi',
                    'source'        => 'qiwi',
                    'source_id'     => $inputs["txn_id"]
                ]);
                $this->calcUserAmount($user->id);

                $xml = '<?xml version="1.0" encoding="utf-8"?>
                        <response>
                        <osmp_txn_id>' . $inputs['txn_id'] . '</osmp_txn_id>
                        <result>0</result>
                        <sum>' . $inputs["sum"] . '</sum>
                        <comment>ok</comment>
                        </response>
                        ';
                return response($xml, 200)->header('Content-Type', 'application/xml');
            } else {
                $xml = '<?xml version="1.0" encoding="utf-8"?>
                        <response>
                        <osmp_txn_id>' . $inputs['txn_id'] . '</osmp_txn_id>
                        <result>5</result>
                        <comment>' . $this->response_codes_qiwi[5] . '</comment>
                        </response>
                        ';
                return response($xml, 200)->header('Content-Type', 'application/xml');
            }
        }
    }

    public function kaspi(Request $request)
    {
        $ips = array(
            "89.218.54.34",
            "89.218.54.36",
            "212.154.215.82",
            "79.142.55.227",
            "202.21.106.125",
            "90.143.35.133",
            '103.10.21.60',
            '66.181.185.172',
            '185.146.1.121',
            '66.181.191.45'
        );



        //   if (!in_array($request->ip(), $ips)) {
        //       return abort(403, 'Unauthorized action.');
        //   }

        $inputs = Input::all();
        if (isset($inputs['command']) && $inputs['command'] == 'check') {
            if (
                isset($inputs['txn_id']) &&
                isset($inputs['account']) &&
                isset($inputs['sum'])
            ) {
                $user_exist = DB::table("users")->where("unumber", '=', $inputs["account"])->first();
                if (!$user_exist) {
                    $xml = '<?xml version="1.0" encoding="utf-8"?>
                          <response>
                          <kaspi_txn_id>' . $inputs['txn_id'] . '</kaspi_txn_id>
                          <sum>' . $inputs['sum'] . '</sum>
                          <result>1</result>
                          <comment>"абонент/счёт не найден" или "заказ не найден", если запрос check был на проверку заказа</comment>
                          </response>
                          ';
                    return response($xml, 200)->header('Content-Type', 'application/xml');
                }


                $user = DB::table("transaction")->where('source', 'kaspi')->where("source_id", '=', $inputs["txn_id"])->first();

                if ($user) {
                    $xml = '<?xml version="1.0" encoding="utf-8"?>
                          <response>
                          <kaspi_txn_id>' . $inputs['txn_id'] . '</kaspi_txn_id>
                          <sum>' . $inputs['sum'] . '</sum>
                          <result>3</result>
                          <comment>заказ уже оплачен</comment>
                          </response>
                          ';
                    return response($xml, 200)->header('Content-Type', 'application/xml');
                } else {
                    $xml = '<?xml version="1.0" encoding="utf-8"?>
                          <response>
                          <kaspi_txn_id>' . $inputs['txn_id'] . '</kaspi_txn_id>
                          <sum>' . $inputs['sum'] . '</sum>
                          <result>0</result>
                          <comment></comment>
                          </response>
                          ';
                    return response($xml, 200)->header('Content-Type', 'application/xml');
                }
            } else {
                $xml = '<?xml version="1.0" encoding="utf-8"?>
                          <response>
                          <kaspi_txn_id>' . $inputs['txn_id'] . '</kaspi_txn_id>
                          <sum>' . $inputs['sum'] . '</sum>
                          <result>1</result>
                          <comment>"абонент/счёт не найден" или "заказ не найден", если запрос check был на проверку заказа</comment>
                          </response>
                          ';
                return response($xml, 200)->header('Content-Type', 'application/xml');
            }
        }
        if (isset($inputs['command']) && $inputs['command'] == 'pay') {
            if (
                isset($inputs['txn_id']) &&
                isset($inputs['account']) &&
                isset($inputs['sum']) &&
                isset($inputs['txn_date'])
            ) {
                $user = DB::table("users")->where("unumber", '=', $inputs["account"])->first();
                if ($user && is_numeric($inputs["sum"])) {

                    $isactive = DB::table("transaction")->where('source', 'kaspi')->where('source_id', '=', $inputs["txn_id"])->get();

                    if (count($isactive) > 0) {
                        $xml = '<?xml version="1.0" encoding="utf-8"?>
                              <response>
                              <kaspi_txn_id>' . $inputs['txn_id'] . '</kaspi_txn_id>
                              <sum>' . $inputs["sum"] . '</sum>
                              <result>3</result>
                              <comment>заказ уже оплачен</comment>
                              </response>
                              ';
                        return response($xml, 200)->header('Content-Type', 'application/xml');
                    } else {


                        DB::table("transaction")->insert([
                            'user_id'       => $user->id,
                            'amount'        => $inputs['sum'],
                            'status'        => 'income',
                            'description'   => 'terminal: kaspi',
                            'source'        => 'kaspi',
                            'source_id'     => $inputs["txn_id"]
                        ]);
                        $this->calcUserAmount($user->id);

                        $xml = '<?xml version="1.0" encoding="utf-8"?>
                              <response>
                              <kaspi_txn_id>' . $inputs['txn_id'] . '</kaspi_txn_id>
                              <result>0</result>
                              <sum>' . $inputs["sum"] . '</sum>
                              <comment>платёж принят успешно</comment>
                              </response>
                              ';
                        return response($xml, 200)->header('Content-Type', 'application/xml');
                    }
                } else {
                    $xml = '<?xml version="1.0" encoding="utf-8"?>
                          <response>
                          <kaspi_txn_id>' . $inputs['txn_id'] . '</kaspi_txn_id>
                          <result>4</result>
                          <comment>другая ошибка провайдера</comment>
                          </response>
                          ';
                    return response($xml, 200)->header('Content-Type', 'application/xml');
                }
            } else {
                $xml = '<?xml version="1.0" encoding="utf-8"?>
                          <response>
                          <kaspi_txn_id>' . $inputs['txn_id'] . '</kaspi_txn_id>
                          <result>4</result>
                          <comment>другая ошибка провайдера</comment>
                          </response>
                          ';
                return response($xml, 200)->header('Content-Type', 'application/xml');
            }
        }
    }

    public function kassa24(Request $request)
    {
        $ips = array(
            "89.218.54.34",
            "89.218.54.36",
            "212.154.215.82",
            "79.142.55.227",
            "202.21.106.125",
            "90.143.35.133",
            '103.10.21.60',
            '66.181.185.172',
            '185.146.1.121',
            '66.181.191.45',
            '103.229.120.57',
            '80.249.154.1'
        );



        //   if (!in_array($request->ip(), $ips)) {
        //       return abort(403, 'Unauthorized action.');
        //   }

        $inputs = Input::all();
        if (!isset($inputs["action"])) {
            return response()->json([
                'code' => 1,
                'message' => $this->response_codes_kassa24[1]
            ]);
        }
        if (!in_array($inputs["action"], $this->actions_kassa24)) {
            return response()->json([
                'code' => 1,
                'message' => $this->response_codes_kassa24[1]
            ]);
        }


        if (isset($inputs['action']) && $inputs["action"] == "check") {

            $user = DB::table("users")->where("unumber", $inputs["number"])->where("deleted", 0)->first();
            if ($user) {
                return response()->json([
                    'code' => 0,
                    'message' => $this->response_codes_kassa24[0]
                ]);
            } else {
                return response()->json([
                    'code' => 2,
                    'message' => $this->response_codes_kassa24[2]
                ]);
            }
        }


        if (isset($inputs['action']) && $inputs["action"] == "payment") {
            $user = DB::table("users")->where("unumber", $inputs["number"])->where("deleted", 0)->first();
            if ($user) {
                if ($this->validateAmount($inputs['amount'])) {
                    if ($this->validateReceipt($inputs['receipt'])) {
                        if ($this->validateDate($inputs['date'])) {
                            DB::table("transaction")->insert([
                                'user_id'       => $user->id,
                                'amount'        => $inputs['amount'],
                                'status'        => 'income',
                                'description'   => 'terminal: kassa24',
                                'source'        => 'kassa24',
                                'source_id'     => $inputs["receipt"]
                            ]);
                            $this->calcUserAmount($user->id);
                            return response()->json([
                                'code' => 0,
                                'message' => $this->response_codes_kassa24[0]
                            ]);
                        } else {
                            return response()->json([
                                'code' => 5,
                                'message' => $this->response_codes_kassa24[5]
                            ]);
                        }
                    } else {
                        return response()->json([
                            'code' => 4,
                            'message' => $this->response_codes_kassa24[4]
                        ]);
                    }
                } else {
                    return response()->json([
                        'code' => 3,
                        'message' => $this->response_codes_kassa24[3]
                    ]);
                }
            } else {
                return response()->json([
                    'code' => 2,
                    'message' => $this->response_codes_kassa24[2]
                ]);
            }
        }
    }

    private function calcUserAmount($user_id)
    {
        $total = DB::table("transaction")->where('user_id', $user_id)->whereIn('status', ['income', 'outcome'])->sum("amount");
        DB::table("users")->where('id', $user_id)->update(['amount' => $total]);
    }

    private function validateAmount($amount)
    {
        if (!empty($amount)) {
            if (preg_match('/^\d{1,6}\.\d{2}$/', $amount) === 1)
                return true;
        }
        return false;
    }


    private function validateReceipt($receipt)
    {
        if (!empty($receipt)) {
            if (preg_match('/\d+/', $receipt) === 1)
                return true;
        }
        return false;
    }


    private function validateDate($date)
    {
        if (!empty($date)) {
            if (preg_match('/\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}/', $date) === 1)
                return true;
        }
        return false;
    }
}
