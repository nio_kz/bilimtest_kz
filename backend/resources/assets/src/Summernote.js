module.exports = {

    template: '<textarea :name="name"></textarea>',

    props: {
        model: {
            required: true,
        },

        name: {
            type: String,
            required: true,
        },

        height: {
            type: String,
            default: '150'
        }
    },

    mounted() {
        let config = {
            height: this.height
        };

        let vm = this;

        config.callbacks = {

            onInit: function () {
                $(vm.$el).summernote("code", vm.model);
            },

            onChange: function () {
                vm.$emit('change', $(vm.$el).summernote('code'));
            },

            onBlur: function () {
                vm.$emit('change', $(vm.$el).summernote('code'));
            },

            onImageUpload: function (files) {
                url = "/api-admin/upload-summernote";
                sendFile(files[0], url, $(this));
            }
        };

        function sendFile(file, url, editor) {
            let api_token = document.head.querySelector('meta[name="api-token"]');
            var data = new FormData();
            data.append("file", file);
            var request = new XMLHttpRequest();
            request.open('POST', url, true);
            request.setRequestHeader("Authorization", 'Bearer ' + api_token.content);
            request.onload = function () {
                if (request.status >= 200 && request.status < 400) {
                    // Success!
                    var resp = request.responseText;
                    editor.summernote('insertImage', resp);
                    console.log(resp);
                } else {
                    // We reached our target server, but it returned an error
                    var resp = request.responseText;
                    console.log(resp);
                }
            };
            request.onerror = function (jqXHR, textStatus, errorThrown) {
                // There was a connection error of some sort
                console.log(jqXHR);
            };
            request.send(data);
        }
        $(this.$el).summernote(config);

    },
    methods: {
        /**
         * run summernote API
         * @param {String} code
         * @param {String | Number} value
         * @returns {*|jQuery}
         */
        run: function (code, value) {
            if (typeof value === undefined) {
                return $(this.$el).summernote(code)
            } else {
                return $(this.$el).summernote(code, value)
            }
        }
    }

};