// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
require('./bootstrap');
import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import Notifications from 'vue-notification'
import velocity from 'velocity-animate'
import App from './App.vue'
import router from './router'
import moment from 'moment'
import Summernote from "./Summernote";
import Pagination from "./components/Pagination.vue";
import VueResource from "vue-resource";
Vue.component('pagination', Pagination);

// import * as VueGoogleMaps from 'vue2-google-maps' 

// Vue.use(VueGoogleMaps, {
//   load: {
//     key: 'AIzaSyCn7TTdXqDjq1ngJi8JCEb2znvF5uOeRTY',
//     libraries: 'places', // This is required if you use the Autocomplete plugin
//     // OR: libraries: 'places,drawing'
//     // OR: libraries: 'places,drawing,visualization'
//     // (as you require)
//   }
// })

Vue.use(BootstrapVue);
Vue.use(VueResource);
Vue.component('summernote' , Summernote );
Vue.filter('toCurrency', function (value) {
    value = parseInt(value);
    var formatter = new Intl.NumberFormat();
    return formatter.format(value) + "₮";
});


Vue.filter('status_class', function (status) {
    return status === 'Active' ? 'badge-success'
        : status === 'Inactive' ? 'badge-secondary'
            : status === 'Pending' ? 'badge-warning'
                : status === 'New' || status === 'new' ? 'badge-success'
                    : status === false || status === 'not-sent' ? 'badge-warning'
                        : status === true || status === 'sent' ? 'badge-success'
                            : status === 'approved' ? 'badge-primary'
                                : status === 'denied' ? 'badge-danger'
                                    : status === 'recevied' ? 'badge-success'
                                        : status === 'Banned' ? 'badge-danger' : 'badge-primary'
});


Vue.filter('status_text', function (status) {
    return status === 'Active' ? 'Идэвхтэй'
        : status === 'Inactive' ? 'Идэвхгүй'
            : status === 'Pending' ? 'Хүлэгдэж байна'
                : status === 'New' || status === 'new' ? 'Шинэ'
                    : status === false || status === 'not-sent' ? 'Илгээгдээгүй'
                        : status === true || status === 'sent' ? 'Илгээсэн'
                            : status === 'approved' ? 'Баталгаажуулсан '
                                : status === 'denied' ? 'Цуцлагдсан '
                                    : status === 'recevied' ? 'Хүлээж авсан'
                                        : status === 'Banned' ? 'Түгжсэн' : 'Амжиллтай'
});
Vue.filter('formatDate', function(value) {
    if (value) {
        return moment(String(value)).format('MM/DD/YYYY')
    }
});
Vue.filter('date_format', function (value) {

    if (value) {
        return moment(String(value)).format('YYYY-DD-MM hh:mm')
    }
    return "---- -- --"
});


Vue.use(Notifications, {velocity});
import Vuex from "vuex";

Vue.use(Vuex);

const store = new Vuex.Store({
    state: {
        settings: []
    },
    mutations: {
        setSettings(state, value) {
            state.settings = value;
        }
    }
});

new Vue({
    router,
    template: '<App/>',
    components: {
        App
    },
    store: store
}).$mount('#app');
