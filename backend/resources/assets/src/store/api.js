
// const API_URL = "http://localhost:88/api-admin/";
// const API_ROOT = "http://localhost:88";


const API_URL = "http://api.megatalant.kz/api-admin/";
const API_ROOT = "http://api.megatalant.kz";

const SUCCESS_ADDED = "Сәтті тіркелді.";
const SUCCESS_EDITED = "Сәтті өзгертілді.";
const SUCCESS_SENT = "Сәтті жіберілді";
const SUCCESS_SAVED = "Сәтті сақталды.";
const SUCCESS_REMOVE = "Сәтті өшірілді.";
const SUCCESS_LOADED = "successfuly loaded data";
const ERROR_ADD = "Тіркеу барысында қате шықты!";
const ERROR_EDIT = "Түзету барысында қате шықты!!";
const ERROR_SAVE = "Сақтау барысында қате шықты!";
const ERROR_SENT = "Жіберу барысында қате шықты!";
const ERROR_OTHER = "Серверден қателік шықты!";
const ERROR_REMOVE = "Өшіру барысында қателік шықты!";
const ERROR_LOADED = "error loading data!";

export { API_URL, 
	API_ROOT, 
	SUCCESS_ADDED, 
	SUCCESS_EDITED, 
	SUCCESS_SAVED, 
	SUCCESS_SENT,
	SUCCESS_REMOVE,
	SUCCESS_LOADED,
	ERROR_SENT,
	ERROR_SAVE,
	ERROR_EDIT, 
	ERROR_ADD,
	ERROR_REMOVE,
	ERROR_OTHER,
	ERROR_LOADED

};