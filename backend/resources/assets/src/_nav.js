export default {
    items: [
        
        {
            name: 'Olimpiad ',
            title: true
        },
        {
            name: 'Quizes',
            url: '/quizes',
            icon: 'fas fa-cart-arrow-down'
        },
        {
            name: 'Lessons',
            url: '/lessons',
            icon: 'fas fa-cart-arrow-down'
        },
        {
            name: 'Classes',
            url: '/classes',
            icon: 'fas fa-cart-arrow-down'
        },
        {
            name: 'Questions',
            url: '/questions',
            icon: 'fas fa-cart-arrow-down'
        },
        {
            name: 'Random Questions',
            url: '/random-questions',
            icon: 'fas fa-cart-arrow-down'
        },
        // {
        //     name: 'Lesson Plan ',
        //     title: true
        // },
        // {
        //     name: 'Lesson Plans',
        //     url: '/lesson-plan',
        //     icon: 'fas fa-cart-arrow-down'
        // },
        // {
        //     name: 'LP Lesson',
        //     url: '/lp-lesson',
        //     icon: 'fas fa-cart-arrow-down'
        // },
        // {
        //     name: 'LP Class',
        //     url: '/lp-class',
        //     icon: 'fas fa-cart-arrow-down'
        // },
        // {
        //     name: 'LP Plan Type',
        //     url: '/lp-plan-type',
        //     icon: 'fas fa-cart-arrow-down'
        // },
        // {
        //     name: 'Konkurs ',
        //     title: true
        // },
        // {
        //     name: 'Konkurs',
        //     url: '/konkurs',
        //     icon: 'fas fa-cart-arrow-down'
        // },

        {
            name: 'Report ',
            title: true
        },
        {
            name: 'Reports',
            url: '/reports',
            icon: 'fas fa-cart-arrow-down'
        },
        {
            name: 'Test ',
            title: true
        },
        {
            name: 'Invoice',
            url: '/invoice',
            icon: 'fas fa-cart-arrow-down'
        },
        {
            name: 'Certificates',
            url: '/certificates',
            icon: 'fas fa-cart-arrow-down'
        },
        // {
        //     name: 'Invoice',
        //     url: '/invoice',
        //     icon: 'fas fa-cart-arrow-down'
        // },
        {
            name: 'News',
            url: '/news',
            icon: 'fas fa-cart-arrow-down'
        },
        // {
        //     name: 'User Quiz',
        //     url: '/user-quiz',
        //     icon: 'fas fa-cart-arrow-down'
        // },
        {
            name: 'Users',
            url: '/users',
            icon: 'fas fa-cart-arrow-down'
        },
        {
            name: 'Schools',
            url: '/schools',
            icon: 'fas fa-cart-arrow-down'
        },
       
        // {
        //     name: 'Income & Outcome',
        //     url: '/income-outcome',
        //     icon: 'fas fa-cart-arrow-down'
        // },
        // {
        //     name: 'Reports',
        //     url: '/reports',
        //     icon: 'fas fa-cart-arrow-down'
        // },
        // {
        //     name: 'Translations',
        //     url: '/translations',
        //     icon: 'fas fa-cart-arrow-down'
        // },
        //  {
        //     name: 'ConvertData',
        //     url: '/convert-data',
        //     icon: 'fas fa-cart-arrow-down'
        // }
        {
            name: 'Konkurs ',
            title: true
        },
        {
            name: 'Settings',
            url: '/settings',
            icon: 'fas cogs'
        },
        {
            name: 'Winners',
            url: '/winners',
            icon: 'fas star'
        },
    ]
}
