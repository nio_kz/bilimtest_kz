
window._ = require('lodash');

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
window.axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*';

axios.interceptors.response.use(function (response) {
    // Do something with response data
        if(response.status==401){
            
            window.location = "/logout"
        }
        console.log(response.status);
        if(response.status==404){
            Vue.prototype.$notify({ group: 'foo', type:'success', text:"Data not found"})
        }
    return response;
  }, function (error, a) {
    if (error.response.status === 401) {
        console.log('unauthorized, logging out ...');
        window.location = "/logout"
    }
    return Promise.reject(error.response);
    //window.location="/?status=expired"
  });

/**
 * Next we will register the CSRF Token as a common header with Axios so that
 * all outgoing HTTP requests automatically have it attached. This is just
 * a simple convenience so we don't have to attach every token manually.
 */

let token = document.head.querySelector('meta[name="csrf-token"]');
let data = document.head.querySelector('meta[name="api-token"]');

if (data){
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + data.content;
}else{
    console.error('jwt token not valid');
}
//
// if (token) {
//     window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
// } else {
//     console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
// }

/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */

// import Echo from 'laravel-echo'

// window.Pusher = require('pusher-js');

// window.Echo = new Echo({
//     broadcaster: 'pusher',
//     key: 'your-pusher-key'
// });
