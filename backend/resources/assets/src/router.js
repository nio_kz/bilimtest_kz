import Vue from 'vue'
import Router from 'vue-router'

// Containers
import Full from './containers/Full'

// Views
import Dashboard from './views/Dashboard'
import Translation from './views/Translation'
import Quizes from './views/Quizes'
import Certificate from './views/Certificate.vue'
import Lessons from './views/Lessons.vue'
import Classes from './views/Classes.vue'

import LPLessons from './views/LPLessons.vue'
import LPClasses from './views/LPClasses.vue'
import LPPlanType from './views/LPPlanType.vue'

import Questions from './views/Questions.vue'
import Users from './views/Users.vue'
import Invoice from './views/Invoice.vue'
import School from './views/School.vue'
import RandomQuestions from './views/RandomQuestions.vue'
import ConvertData from './views/ConvertData.vue'
import UserQuiz from './views/UserQuiz.vue'
import UserQuizResult from './views/UserQuizResult.vue'
import News from './views/News.vue'
import LessonPlan from './views/LessonPlan.vue'
import RP from './views/RP.vue'
import Konkurs from './views/Konkurs.vue'
import KonkursMembers from './views/KonkursMembers.vue'
import Settings from './views/Settings.vue'
import Winners from './views/Winners.vue'

Vue.use(Router);

export default new Router({
    mode: 'hash',
    linkActiveClass: 'open active',
    scrollBehavior: () => ({y: 0}),
    routes: [
        {
            path: '/',
            redirect: '/dashboard',
            name: 'Home',
            meta: {'label': 'Нүүр',},
            component: Full,
            children: [
                {
                    path: 'dashboard',
                    name: 'Dashboard',
                    meta: {'label': 'Дашборд'},
                    component: Dashboard
                },
                {
                    path: 'translation',
                    name: 'Translation',
                    meta: {'label': 'Translation'},
                    component: Translation
                },
                {
                    path: 'quizes',
                    name: 'quizes',
                    meta: {'label': 'Quizes'},
                    component: Quizes,

                },
                {
                    path: 'lessons',
                    name: 'lessons',
                    meta: {'label': 'Lessons'},
                    component: Lessons,

                },
                {
                    path: 'news',
                    name: 'news',
                    meta: {'label': 'News'},
                    component: News,

                },
                {
                    path: 'classes',
                    name: 'classes',
                    meta: {'label': 'Classes'},
                    component: Classes,

                },
                {
                    path: 'lp-lesson',
                    name: 'lp-lesson',
                    meta: {'label': 'LP Lesson'},
                    component: LPLessons,

                },
                {
                    path: 'lp-class',
                    name: 'lp-class',
                    meta: {'label': 'LP Classes'},
                    component: LPClasses,

                },
                {
                    path: 'lp-plan-type',
                    name: 'lp-plan-type',
                    meta: {'label': 'LP Type'},
                    component: LPPlanType,

                },
                {
                    path: 'lesson-plan',
                    name: 'lesson-plan',
                    meta: {'label': 'LessonPlan'},
                    component: LessonPlan,

                },
                {
                    path: 'questions',
                    name: 'Questions',
                    meta: {'label': 'Questions'},
                    component: Questions
                },
                {
                    path: 'random-questions',
                    name: 'random-questions',
                    meta: {'label': 'Random Questions'},
                    component: RandomQuestions
                },
                {
                    path: 'certificates',
                    name: 'certificates',
                    meta: {'label': 'Certificates'},
                    component: Certificate
                },
                {
                    path: 'invoice',
                    name: 'invoice',
                    meta: {'label': 'Invoice'},
                    component: Invoice
                },
                {
                    path: 'user-quiz',
                    name: 'user_quiz',
                    meta: {'label': 'User Quiz'},
                    component: Dashboard
                },
                {
                    path: 'users',
                    name: 'users',
                    meta: {'label': 'Users'},
                    component: Users
                },
                {
                    path: 'schools',
                    name: 'schools',
                    meta: {'label': 'Schools'},
                    component: School
                },
                {
                    path: 'income-outcome',
                    name: 'income_outcome',
                    meta: {'label': 'Income & Outcome'},
                    component: Dashboard
                },
                {
                    path: 'reports',
                    name: 'reports',
                    meta: {'label': 'Reports'},
                    component: RP
                },
                {
                    path: 'translations',
                    name: 'translations',
                    meta: {'label': 'Translation'},
                    component: Translation
                },
                {
                    path: 'convert-data',
                    name: 'ConvertData',
                    meta: {'label': 'ConvertData'},
                    component: ConvertData
                },
                {
                    path: 'user-quiz/:user_id',
                    name: 'user-quiz',
                    meta: {'label': 'User Quiz'},
                    component: UserQuiz
                },
                {
                    path: 'user-quiz-result/:uq',
                    name: 'user-quiz-result',
                    meta: {'label': 'User Quiz Result'},
                    component: UserQuizResult
                },{
                    path: 'konkurs',
                    name: 'konkurs',
                    meta: {'label': 'Konkurs'},
                    component: Konkurs
                },{
                    path: 'konkurs/:id/members',
                    name: 'konkurs-members',
                    meta: {'label': 'KonkursMembers'},
                    component: KonkursMembers
                }
                ,{
                    path: 'settings',
                    name: 'settings',
                    meta: {'label': 'Settings'},
                    component: Settings
                },{
                    path: 'winners',
                    name: 'winners',
                    meta: {'label': 'Winners'},
                    component: Winners
                }

            ]
        }
    ]
})
