<div
    style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box;background-color:#f5f8fa;color:#74787e;height:100%;line-height:1.4;margin:0;width:100%!important;word-break:break-word">

    <table width="100%" cellpadding="0" cellspacing="0"
        style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box;background-color:#f5f8fa;margin:0;padding:0;width:100%">
        <tbody>
            <tr>
                <td align="center" style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box">
                    <table width="100%" cellpadding="0" cellspacing="0"
                        style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box;margin:0;padding:0;width:100%">
                        <tbody>
                            <tr>
                                <td
                                    style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box;padding:25px 0;text-align:center">
                                    <a href="https://www.edulife.kz"
                                        style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box;color:#bbbfc3;font-size:19px;font-weight:bold;text-decoration:none"
                                        target="_blank">
                                        
                                        Edulife.kz
                                    </a>
                                </td>
                            </tr>
                            <tr>
                              
                                <td width="100%" cellpadding="0" cellspacing="0"
                                    style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box;background-color:#ffffff;border-bottom:1px solid #edeff2;border-top:1px solid #edeff2;margin:0;padding:0;width:100%">
                                    <table align="center" width="570" cellpadding="0" cellspacing="0"
                                        style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box;background-color:#ffffff;margin:0 auto;padding:0;width:570px">
                                        <tbody>
                                            <tr>
                                                <td
                                                    style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box;padding:35px">
                                                    <h1
                                                        style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box;color:#2f3133;font-size:19px;font-weight:bold;margin-top:0;text-align:left">
                                                        Сәлеметсіз бе?!</h1>
                                                    <p
                                                        style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box;color:#74787e;font-size:16px;line-height:1.5em;margin-top:0;text-align:left">
                                                        Құпия сөзді қалпына келтіру. Егерде бұл өтінішіті сіз жібермеген болсаңыз,құпия сөзді қалпына келтіру міндетті емес екендігін ескертеміз.</p>
                                                    <table align="center" width="100%" cellpadding="0" cellspacing="0"
                                                        style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box;margin:30px auto;padding:0;text-align:center;width:100%">
                                                        <tbody>
                                                            <tr>
                                                                <td align="center"
                                                                    style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box">
                                                                    <table width="100%" border="0" cellpadding="0"
                                                                        cellspacing="0"
                                                                        style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td align="center"
                                                                                    style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box">
                                                                                    <table border="0" cellpadding="0"
                                                                                        cellspacing="0"
                                                                                        style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box">
                                                                                        <tbody>
                                                                                            <tr>
                                                                                                <td
                                                                                                    style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box">
                                                                                                    <a href="https://www.edulife.kz/user/password-reset?token={{$token}}"
                                                                                                        style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box;border-radius:3px;color:#fff;display:inline-block;text-decoration:none;background-color:#3097d1;border-top:10px solid #3097d1;border-right:18px solid #3097d1;border-bottom:10px solid #3097d1;border-left:18px solid #3097d1"
                                                                                                        target="_blank"
                                                                                                        >Құпия сөзді қалпына келтіру</a>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                  
                                                    <p
                                                        style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box;color:#74787e;font-size:16px;line-height:1.5em;margin-top:0;text-align:left">
                                                        Құрметпен, edulife.kz  ұжымы</p>
                                                   
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                           
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
</div>