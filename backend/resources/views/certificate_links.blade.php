<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Links</title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<body>

<div class="container">
    <div class="card mt-5 mb-5">
        <div class="card-header">
        <form method="get" class="form-inline">
            <input type="text" name="name" class="form-control mr-2 w-50" placeholder="Атын немесе тегін жазып 'Іздеу' батырмасына басыңыз. ..." value="{{$name}}" />
            <button class="btn btn-m btn-info mr-2" type="submit">Іздеу</button>
            <a class="btn btn-m btn-warning" href="/links">Қалпына келтіру</a>
            </form>
        </div>
        <div class="card-body">
            <table class="table table-striped table-hovered">
                <tr>
                    <th></th>
                    <th>Аты-жөні</th>
                    <th>PDF (жүктеу) </th>
                    <th>JPG (жүктеу)</th>
                </tr>
                @foreach($items as $item)
                <tr>
                    <td>{{$item->id}}</td>
                    <td>{{$item->full_name}}</td>
                    <td><a href="https://admin.edulife.kz/api/user/konkurs-certificate/pdf/{{$item->id}}/pos/0?download_type=download"  target="blank">PDF (жүктеу) </a></td>
                    <td><a href="https://admin.edulife.kz/api/user/konkurs-certificate/image/{{$item->id}}/pos/0?download_type=download" target="blank" >JPG (жүктеу)</a></td>
                </tr>
                @endforeach
            </table>
        </div>
    </div>
</div>
</body>
</html>