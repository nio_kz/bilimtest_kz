<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="api-token" content="{{ $token }}">
    <title>Control Panel</title>
    <link rel="stylesheet" type="text/css" href="/summernote/summernote-bs4.css">
    <link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" href="/js/pace/themes/blue/pace-theme-flash.css">
</head>
<body class="app header-fixed sidebar-fixed aside-menu-fixed aside-menu-hidden">
@if($token)
    <div id="app"></div>
    <script type="text/javascript" src="/js/jquery-2.1.4.min.js"></script>
    <script type="text/javascript" src="/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/summernote/summernote-bs4.min.js"></script>
    <script src="/js/pace/pace.min.js" data-pace-options='{ "ajax": true }'></script>
    <script src="/js/manifest.js?time={{date("Y-m-d")}}"></script>
    <script src="/js/vendor.js?time={{date("Y-m-d")}}"></script>
    <script src="/js/app.js?time={{date("Y-m-d")}}"></script>
@else
    <div class="container-fluid">
        <div class="app flex-row align-items-center">
            <div class="container">
                <div class="row">
                    <div class="mx-auto col-6">
                        <div class="card">
                            <div class="card-header">
                                Login for admin
                            </div>
                            <div class="card-body">
                                <form action="/login" method="post">
                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input type="email" name="email" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="password">Password</label>
                                        <input type="password" name="password" class="form-control">
                                    </div>
                                    <div class="form-group text-center">
                                        <button type="submit" class="btn btn-primary">Login</button>
                                    </div>
                                </form>
                            </div>
                            <div class="card-footer">

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endif
</body>
</html>