let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/src/app.js', 'public/js')
   .sass('resources/assets/scss/style.scss', 'public/css');
mix.extract([
   'vue', 
    'vuex',  
    'axios',
    'bootstrap-sass', 
   
    
   //  "jquery",
   //  "lodash",
   //  "axios-progress-bar",
    "bootstrap-vue",
    "localforage",
    "moment",
    "velocity-animate",
    "vue-authenticate",
    "vue-awesome",
    "vue-html-editor",
    "vue-multilanguage",
    "vue-notification",
    "vue-resource",
    "vue-router",
    "vue-template-compiler",
    "vue2-google-maps",
    "vue2-notify",
    "vuejs-datepicker",
    "vuejs-paginator",
    
]).version();
