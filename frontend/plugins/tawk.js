import Tawk from 'vue-tawk'
import Vue from 'vue'

Vue.use(Tawk, {
    tawkSrc: 'https://embed.tawk.to/6020c1ffa9a34e36b974cf58/1etvvbk5o'
})