import axios from './axios'

export default ({ app, store, $axios }) => {
  app.router.afterEach((to, from) => {
    
    store.commit('setUnfinished', [])

    if (app.$auth.loggedIn) {
      if (to.name != 'olimpiad-id') {
        $axios.get('/unfinished').then((res) => {
          store.commit('setUnfinished', res.data)
        })
      }
    }   
  })
}
