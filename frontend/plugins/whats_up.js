(function () {
    var options = {
        whatsapp: "+77057683113", // WhatsApp number
        call_to_action: "Сұрағыңыз болса хат жазыңыз!", // Call to action
        // company_logo_url: "http://localhost:3000/images/logo.jpg", // URL of company logo (png, jpg, gif)
        // greeting_message: "Сәлеметсіз! Бізге whatsapp-та хабарлас.\n\nЗдраствуйте! Напишите нам в whatsapp", // Text of greeting message
        // call_to_action: "Бізге whatsapp-та хабарлас", // Call to action
        button_color: "#129BF4", // Color of button
        position: "left", // Position may be 'right' or 'left'
        order: "whatsapp,vkontakte,facebook,viber" // Order of buttons
    };
    var proto = document.location.protocol, host = "whatshelp.io", url = proto + "//static." + host;
    var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = url + '/widget-send-button/js/init.js';
    s.onload = function () { WhWidgetSendButton.init(host, proto, options); };
    var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x);
})();