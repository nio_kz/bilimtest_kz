


export const state = () => {
  return {
   
    loading: false,
    inlineLoading: false,
    total_questions: 0,
    questions: [],
    show_mobile_menu: false,
    show_mobile_user_menu: false,
    unfinished:[],
    ip:"0.0.0.0"
  }
}
export const mutations = {
  setShowMobileMenu(state, val) {
    state.show_mobile_menu = val
  },
  setUnfinished(state, val) {
    state.unfinished = val
  },
  setShowMobileUserMenu(state, val) {
    state.show_mobile_user_menu = val
  },

  setLoading(state, status) {
    state.loading = status
  },
  setInlineLoading(state, status) {
    state.inlineLoading = status
  },
  setTotalQuestion(state, total) {
    state.total_questions = total
  },
  setQuestion(state, items) {
    state.questions = items
  },
  setQuestionIsChecked(state, item) {
    state.questions[item].is_checked = 1
    
  },
  SET_IP(state, ip) {
    console.log(Buffer.from(ip).toString('base64'))
    state.ip = Buffer.from(ip).toString('base64')
    
  }
}
